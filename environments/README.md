First, get a node in interactive mode:

On the frontend:
```sh
oarsub -I
```
Now that you are connected to a node, you have to install install Kameleon
and the tools needed to run the recipe:
```sh
sudo-g5k gem install kameleon-builder
sudo DEBIAN_FRONTEND=noninteractive apt -y install qemu socat pigz polipo libguestfs-tools
```
To build image on the node:
```sh
cd ~/bebida/environments
kameleon build bebida-slave.yaml -b /tmp
```
To register the environment on the frontend use the commande provided at
the end of the recipe execution **but on the frontend**, i.e.:
```sh
[local] To import the environment to Kadeploy:
[local] kaenv3 -a /home/mmercier/my_g5k_images/bebida-slave_a18cc5729a5f.yaml
```
Then run the experiments using the scripts provided in each experiments
folder.