class env::min::udev {
  package {
    'udev':
      ensure => installed;
  }
  # Files manipulation?
}
