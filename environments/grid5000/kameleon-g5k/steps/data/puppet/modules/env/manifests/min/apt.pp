class env::min::apt {

  # Keyring
  #
  include 'env::min::apt::keyring'

  # Grid'5000 repos
  #
  include 'env::min::apt::grid5000'

}

