with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {};

pkgs.buildEnv {
      name = "build-latex";
      paths = with pkgs; [ texlive.combined.scheme-full rubber ];
}
