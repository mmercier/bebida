with import <nixpkgs> {};

let
  customTex = texlive.combine {
    inherit (texlive)
    scheme-full
    textpos
    fontspec
    marvosym
    todo;
  };
in
  stdenv.mkDerivation rec {
    name = "atosStyleBeamer";
    buildInputs = with pkgs; [
      customTex
      rubber
    ];
    src = ./.;
    slides = "./slides";
    FONTCONFIG_FILE = makeFontsConf { fontDirectories = [ corefonts ]; };
    buildPhase = "xelatex ${slides}.tex";
    installPhase = ''
      mkdir $out
      cp ${slides}.pdf $out
      '';
  }
