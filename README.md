# BeBiDa project

This project is a proof of concept that HPC cluster can manage Big Data
workload in best effort using unused resources.

Experiments are made to run on Grid'5000 in a reproducible manner using
Kameleon generated system images and Execo python script. It is following
the [Popper](https://github.com/systemslab/popper) convention for
generating reproducible papers.

For further information contact me at michael dot mercier at inria.fr
