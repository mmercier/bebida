#! /usr/bin/env nix-shell
#! nix-shell analysis.nix -i bash
#
# You'll need nix to automatically download the dependencies: `curl https://nixos.org/nix/install | sh`

jupyter-notebook ./bebida_results_analysis.ipynb
