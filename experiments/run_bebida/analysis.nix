with import (
  fetchTarball "https://gitlab.inria.fr/vreis/datamove-nix/repository/bebida/archive.tar.gz") {};
#with import (fetchTarball "https://gitlab.inria.fr/vreis/datamove-nix/repository/master/archive.tar.gz") {};
let
  python = let
    packageOverrides = self: super: {
      evalys = evalys.overridePythonAttrs(old: rec {
        src =  pkgs.fetchurl {
          url = "https://gitlab.inria.fr/batsim/evalys/repository/IEEE_BigData_2017/archive.tar.gz";
          sha256 = "91448300d4d6973bf1fac948fff645c67ac0153bc5c14d6afaa51f6bad6174ce";
        };
      });
    };
  in
    pkgs.python36.override {inherit packageOverrides;};
in
  (python.withPackages (ps: with ps; [
      jupyter
      evalys
      pip
    ])).env

