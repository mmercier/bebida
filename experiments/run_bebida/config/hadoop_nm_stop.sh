#!/bin/bash

# This script must bu run as hadoop user
nodes=$1

# Stop NodeManagers
for node in $nodes
do
  echo $node >> $HADOOP_CONF_DIR/yarn.exclude
done

# Update yarn nodemanager state
$HADOOP_HOME/bin/yarn rmadmin -refreshNodes
