import sys
import os
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import json
import expe_tools
from expe_tools import mount_storage5k, generate_data, load_experiment_config, get_resources, deploy_images
from execo import Remote
from execo_engine import Engine, logger

script_path = os.path.dirname(os.path.realpath(__file__))


class DatasetGeneration(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--force_redeploy',
                          help='can be all, master, slaves')
        parser.add_option('--oar_job_id',
                          help="Grid'5000 reservation job ID")

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None

        # get configuration
        config = load_experiment_config(self.args[0], self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])
        env = config["kadeploy_env"]
        workloads = config["workloads"]
        data_dir = config.get("data_dir", None)
        logger.info(config)

        # check if workloads exists (Suppose that the same NFS mount point
        # is present on the remote and the local environment
        for workload_file in workloads:
            with open(workload_file):
                pass

        job_id, nodes = get_resources(resources, walltime, site,
                                      oar_job_id=oar_job_id)

        # do deployment
        deploy_images([], None, nodes, env,
                      force_redeploy=self.options.force_redeploy)

        mount_storage5k(nodes, data_dir)

        # Generate data that are missing from all available profiles
        # Fix BigDataBench permissions
        Remote("chmod -R 777 /opt/BigDataBench_V3.2.5_Spark/SNS/Kmeans/Image_data/",
               nodes, connection_params={'user': 'root'}).run()

        # get all profiles
        profiles = {}
        for workload_file in workloads:
            with open(workload_file) as wl:
                profiles = dict(profiles, **json.load(wl)["profiles"])

        logger.info("Input profiles {}".format(profiles.keys()))

        # Do the that generation
        processes = []
        i = 0
        for profile in profiles.values():
            node = nodes[i % (len(nodes) - 1)]
            process = generate_data(node, profile["data_size"],
                          profile["application_type"],
                          data_store=data_dir)
            if process is not None:
                i = i + 1
                processes.append(process)
        for process in processes:
            process.wait()
            if not process.ok:
                raise Exception("Error while generating data: {}".format(process.stderr))

        logger.info("Dataset generation finished successfuly")


if __name__ == "__main__":
    engine = DatasetGeneration()
    expe_tools.script_path = script_path
    engine.start()
