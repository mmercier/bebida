# Big Dataset Management

We have the problem of generating and staging the Big Data datasets on
Grid'5000 that didn't scale well if implemented naively.

For now it is done one only one node and following theses steps:
1) the master is generating data using BigDataBench dataset generation
tools
2) then push it in the NFS reserved and mounted from storage5k
3) finally push it to HDFS

This take way too much time and must be split in separate procedures steps
with 1 and 2 together at the begining of an experiment campaign and 3 at the
beginning of each experiments.

To speedup theses 2 process we need to identify the bottleneck. This is
highly dependent on the hardware setup. In this we use the Graphene cluster
with 1Gb Ethernet with a 4 core CPU (Intel(R) Xeon(R) CPU X3440 @ 2.53GHz)
and 15G of memory.

## Dataset Generation procedure

We identify that the bottle neck for the fist step is the CPU: More
precisely SNS Kmeans dataset is generated using only 1 core and the
MicroBench dataset is parallelize on the 4 cores but the network link is
only saturated periodically and for small time-lapse.

In order to parallelize the generation we want to create a dedicated
procedure of dataset generation with multiple master-like node, that have
the BigDataBench tools mandatory for the data generation, with one node per
dataset to generate.

Use the following script to generate the dataset needed for one or several
workloads and put it in the the provided data store

```sh
generate_dataset.py expe_graphene_test.json
```

## Populate HDFS

The HDFS population must take place every time a new experiment runs,
after the nodes deployment. Doing it only from the master node sequentially
takes several hours to push 1TB of data which pretty common in Big Data
setup. The bottleneck here is the write bandwidth of the targeted datanode
disk (60M~) but if we parallelize on the master node the network will
quickly become the bottleneck (120M~).

So, the idea is to copy run a copy in parallel from several datanode
directly, one per dataset and reach the NFS max bandwidth.

Using ``copyFromLocal`` HDFS command is not very efficient and leave the
cluster unbalanced. There is a special method with:
```
$HADOOP_HOME/bin/hadoop distcp -m 50 -atomic file:///data/mmercier_1238983/data-SNS-Kmeans-64 test_from_graphene20
```
But it turn out that using this method the HDFS cluster is also unbalanced
because all the data are stored locally.

This balance can be achieved using the HDFS balancer:
```
$HADOOP_HOME/bin/hdfs balancer
```
But it only balance the data from overutilized tu underutilize disks but
all the block of one dataset is possibly on the same node and can create a
big bottleneck.



To do this we use the push to  HDFS method provided in
``../expe_tools.py``.

