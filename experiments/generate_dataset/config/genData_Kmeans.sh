#!/usr/bin/env bash
##
# Use the WIKI dataset, trains and tests a cluster.
# Need HADOOP and MAHOUT
# To prepare and generate data:
# ./genData_Kmeans.sh
# To run:
# ./run_Kmeans.sh
##

set -o errexit
set -o xtrace

WORK_DIR=$1
DATA_FILE=$2

echo "Preparing Kmeans data dir"
mkdir -p ${WORK_DIR}

echo "print data size GB :"
read GB
a=${GB}
let L=a*115000
./Generating Image_data/color100.txt $L > ${WORK_DIR}/$DATA_FILE
