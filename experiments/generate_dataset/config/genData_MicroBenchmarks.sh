#!/usr/bin/env bash
##
# Micro Benchmarks Workload: sort, grep, wordcount
# Need HADOOP 
# To prepare and generate data:
#  ./genData_MicroBenchmarks.sh
# To run:  
#  ./run_MicroBenchmarks.sh
##

set -o errexit
set -o xtrace

echo "Preparing MicroBenchmarks data dir"

WORK_DIR=$1
DATA_DIR=$2
echo "WORK_DIR=$WORK_DIR data will be put in $WORK_DIR/$DATA_DIR"
mkdir -p ${WORK_DIR}/$DATA_DIR
cd ../BigDataGeneratorSuite/Text_datagen/

echo "print data size GB :"
read GB
a=${GB}
let L=a*2
./gen_text_data.sh lda_wiki1w $L 8000 10000 ${WORK_DIR}/$DATA_DIR
