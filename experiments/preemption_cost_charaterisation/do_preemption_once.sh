#
# BeBiDa project experiments about prologue and epilogue overhead on RJMS
# jobs start and stop
#
# Second part where we actually time the prologue and epilogue overhead
#
# WARNING: This must bu run as OAR user

set -x
set -e

nodes=$1
result_dir=${2:-$PWD}
time_to_wait=$3
nb_preempted_nodes=$(wc -w <<< "$nodes")
nb_running_jobs=1

sleep $time_to_wait
/usr/bin/time -f "prologue $nb_preempted_nodes $nb_running_jobs %e" -a -o \
        $result_dir/preemption_cost.csv \
        /etc/oar/server_prologue $result_dir "$nodes"
