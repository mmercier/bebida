#
# BeBiDa project experiments about prologue and epilogue overhead on RJMS
# jobs start and stop
#
# Second part where we actually time the prologue and epilogue overhead
#
# WARNING: This must bu run as OAR user

set -x
set -e

nodes=$1
result_dir=${2:-$PWD}
nb_nodes=$(wc -w <<< "$nodes")

for i in {1..5}
do
/usr/bin/time -f "prologue $nb_nodes $nb_running_jobs %e" -a -o \
        $result_dir/bebida_rjms_job_start_overhead.csv \
        /etc/oar/server_prologue $nodes

sleep 5
/usr/bin/time -f "epilogue $nb_nodes $nb_running_jobs %e" -a -o \
        $result_dir/bebida_rjms_job_start_overhead.csv \
        /etc/oar/server_epilogue $nodes
sleep 10

done
