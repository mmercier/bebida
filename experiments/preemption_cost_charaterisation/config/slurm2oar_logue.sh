#!/bin/bash

# OAR *logue are called with:
# Script is run under uid of oar who is sudo
# argv[1] is the jobid
# argv[2] is the user's name
# argv[3] is the file which contains the list of nodes used
# argv[4] is the job walltime in seconds

echo $(scontrol show hostnames=$SLURM_JOB_NODELIST) > /tmp/SLURM_logue_hosts_$SLURM_JOB_ID

SCRIPT_TO_CALL=ASCRIPTTOCALL

$SCRIPT_TO_CALL $SLURM_JOB_ID $SLURM_JOB_USER /tmp/SLURM_logue_hosts_$SLURM_JOB_ID 999999

exit
