NM_PID=$(pgrep -u hadoop -f org.apache.hadoop.yarn.server.nodemanager.NodeManager)
if [ -z $NM_PID ]
then
  echo "WARNING: The node manager is not running (already stopped)"
else
  echo "Waiting for the node manager to be stopped (PID=$NM_PID)"
  while [ -e /proc/$NM_PID ]; do sleep 0.1; done
fi

exit 0
