#!/bin/bash

# This script must bu run as hadoop user
nodes=$1

# Remove node from excluded
for node in $nodes; do
  sed -i /$node/d $HADOOP_CONF_DIR/yarn.exclude
done

# Update yarn nodemanager state
$HADOOP_HOME/bin/yarn rmadmin -refreshNodes
