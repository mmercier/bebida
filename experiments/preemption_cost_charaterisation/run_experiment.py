#!/usr/bin/env python2

from __future__ import print_function
import os
import time
import json
import errno
import math
import re
import sys
from shutil import copy

from pandas import DataFrame
from execo import Process, SshProcess, Remote, format_date, Put, Get, Report
from execo_engine import Engine, logger
from execo_g5k import oarsub, OarSubmission, get_oar_job_nodes, \
        wait_oar_job_start
from execo_g5k.kadeploy import deploy, Deployment
from execo_engine import ParamSweeper, sweep

# For experiments result gathering
import urllib2

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import expe_tools
from expe_tools import \
    get_resources, deploy_images, configure_OAR, configure_SLURM, \
    load_experiment_config, gather_nodes_information



script_path = os.path.dirname(os.path.realpath(__file__))


class BeBiDaExperiment(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--oar_job_id',
                          help="Grid'5000 OAR job ID")
        parser.add_option('--force_reconfig',
                          action='store_true',
                          default=False,
                          help='if set and oar_job_id is set, the cluster'
                          'is re-configured')
        parser.add_option('--force_hdfs_format',
                          action='store_true',
                          default=False,
                          help='if set, HDFS format will be forced')
        parser.add_option('--force_redeploy',
                          action='store_true',
                          default=False,
                          help='if set, Node re-deployment will be forced')
        parser.add_option('--test', dest='is_a_test',
                          action='store_true',
                          default=False,
                          help='prefix the result folder with "test", enter '
                          'a debug mode if fails and remove the job '
                          'afterward, unless it is a reservation')

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir(self.options.is_a_test)

    def run(self):
        """Run the experiment"""
        logger.debug("Start BeBiDaExperiment.run() method")

        # get options
        force_reconfig = self.options.force_reconfig
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None
        force_hdfs_format = self.options.force_hdfs_format
        force_redeploy = self.options.force_redeploy

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # import configuration
        with open(self.args[0]) as config_file:
            config = json.load(config_file)

        # backup configuration
        copy(self.args[0], self.result_dir)

        # load configuration
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])
        slave_env = config["kadeploy_slave_env"]
        master_env = config["kadeploy_master_env"]
        logger.info("You ask for {} resources during {} "
                    "time".format(resources, walltime))
        parameters = config.get("parameters", {})
        #by default we use OAR
        if "rjms" not in config:
            config["rjms"] = "OAR"
        elif config["rjms"] != "SLURM" and config["rjms"] != "OAR":
            print("Error: unsuported RJMS (only SLURM and OAR are supported).")
            exit(1)

        # get ressources
        job_id, nodes = get_resources(
            resources, walltime, site, oar_job_id)

        # master is the first node of the reservation others are slaves
        master = nodes[0]
        slaves = nodes[1:]

        # do deployment
        deploy_images(master, master_env, slaves, slave_env,
                      force_redeploy=force_redeploy)

        # store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)

        # Configuration
        if (not oar_job_id) or force_reconfig:
            # Create temporary directory
            tmp_dir = self.result_dir + '/tmp'
            Process('mkdir ' + tmp_dir).run()
            
            if config["rjms"] == "SLURM":
                configure_SLURM(tmp_dir, self.result_dir, master, slaves,
                          nodes_info)
            else:
                configure_OAR(tmp_dir, self.result_dir, master, slaves,
                          nodes_info)

            # needed for HDFS and Spark setup
            configure_SSH(tmp_dir, master, slaves)

            configure_Hadoop(tmp_dir, master, slaves, nodes_info)

            try:
                format_HDFS(master, force=force_hdfs_format)
            except:
                pass

            #stop_HDFS(master)
            start_HDFS(master)

            configure_Spark(tmp_dir, master, slaves)

            create_HDFS_user_home(master, os.getlogin())

            stop_YARN(master, slaves)
            start_YARN(master, slaves)

            stop_Spark_history_server(master)
            start_Spark_history_server(master)

        if config["rjms"] == "SLURM":
            activate_SLURM_Prologue_Epilogue(master, slaves)
        else:
            activate_OAR_Prologue_Epilogue(tmp_dir, self.result_dir, master, slaves, nodes_info)

        # FIXME: put this in the kameleon recipe
        # install time for experiments
        SshProcess("apt install time", master,
                   connection_params={'user': 'root'}).run()

        # define the iterator over the parameters combinations
        self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                    sweep(parameters))

        # Due to previous (using -c result_dir) run skip some combination
        logger.info('Skipped parameters:' +
                    '{}'.format(str(self.sweeper.get_skipped())))

        logger.info('combinations {}'.format(
            str(self.sweeper.get_remaining())))

        job_id = 0
        results_filepath = self.result_dir + "/results.csv"
        if os.path.isfile(results_filepath):
            logger.warning("Loading previous results")
            result_df = DataFrame.from_csv(results_filepath)
        else:
            result_df = DataFrame()

        while len(self.sweeper.get_remaining()) > 0:
            logger.info('Remaining combinations {}'.format(
                str(len(self.sweeper.get_remaining()))))
            # get expe parameters
            expe = self.sweeper.get_next()

            # only run the experiment if the control expe (without
            # preemption was done
            # if expe["preemption"] != "none" \
            #     and (result_df.empty or not (
            #             (result_df["framework_type"] ==
            #              expe["framework_type"]) &
            #             (result_df["application_name"] ==
            #              expe["application_name"]) &
            #             (result_df["data_size"] == expe["data_size"])
            #             ).any()):
            #     self.sweeper.cancel(expe)
            #     logger.info("Skip this combinaison until we get baseline "
            #                 "time: {}".format(expe))
            #     continue

            # Add a job id
            job_id = job_id + 1
            expe["job_id"] = job_id

            logger.info("Execute experiment: {}".format(expe))

            result_dir = self.result_dir
            nb_nodes = len(slaves)

            # generate data
            generate_data(master, expe["data_size"], expe["application_type"])

            # Remove results dir if necessary
            cmd = "$HADOOP_HOME/bin/hdfs dfs -rm -r -f /user/mmercier/{application_name}-{iteration}-result{job_id}".format(**expe)

            rm_resutl_dir = SshProcess(cmd, master, connection_params={'user': 'root'}, shell=True)
            rm_resutl_dir.run()

            # expand parameters
            expe["application_parameters"] = expe["application_parameters"].format(**expe)

            # --executor-cores 31 \
            # --num-executors {nb_executor} \
            # Submit a Spark job command
            spark_submit_cmd = """/opt/spark/bin/spark-submit \
            --master yarn \
            --deploy-mode client \
            --class cn.ac.ict.bigdatabench.{application_name} \
            /opt/BigDataBench_V3.2.5_Spark/JAR_FILE/bigdatabench-spark_1.3.0-hadoop_1.0.4.jar \
            /data-{application_type}-{data_size} {application_parameters}""".format(**expe)

            # Start spark jobs jobs
            spark_jobs = []
            logger.info("Start Spark jobs...")
            job = SshProcess(spark_submit_cmd, master)
            job.stdout_handlers.append(self.result_dir +
                                       '/spark_job_{}.out'.format(job_id))
            job.stderr_handlers.append(self.result_dir +
                                       '/spark_job_{}.err'.format(job_id))
            logger.info("Spark jobs started")
            spark_jobs.append(job.start())

            # Do preemption
            if expe["preemption"] != "none":

                # generate preemption node_list
                n = int(math.floor(nb_nodes * expe["preempt_resource_ratio"]))
                s = slice(0, n)
                node_list = " ".join([str(node.address) for node in slaves[s]])

                # generate time_to_wait
                time_to_wait = expe["preempt_time_ratio"] * \
                    result_df[
                        (result_df["framework_type"] == expe["framework_type"]) &
                        (result_df["application_type"] == expe["application_type"]) &
                        (result_df["data_size"] == expe["data_size"])
                    ].execution_time.iloc[0]

                logger.info("time to wait before preemption: "
                            "{}".format(time_to_wait))

                cmd = ('su - oar -c \''
                       '{path}/do_preemption_{preemption_type}.sh '
                       '"{node_list}" "{result_dir}" {time}\''.format(
                        path=script_path,
                        preemption_type=expe["preemption"],
                        node_list=node_list,
                        result_dir=result_dir,
                        time=time_to_wait))
                preempt = SshProcess(
                    cmd,
                    master,
                    connection_params={"user": "root"})
                preempt.stdout_handlers.append(
                    '{}/preemption_job_{}.out'.format(result_dir, job_id))
                logger.info("Run preemption on: {}".format(node_list))
                preempt.run()

                expe["preemption_begin"] = preempt.start_date
                expe["preemption_end"] = preempt.end_date

            # Wait for the job to finish
            logger.info("Waiting for job to finish")
            for spark_job in spark_jobs:
                spark_job.wait()
                logger.info("Get application information")
                # register app additional information
                expe["execution_time"] = spark_job.end_date - spark_job.start_date
                expe["starting_time"] = spark_job.start_date
                expe["finish_time"] = spark_job.end_date
                expe["exit_code"] = spark_job.exit_code
                # get application ID from log
                match = re.search(
                    '(?<=Application report for )\w+', spark_job.stdout)
                expe["app_id"] = match.group(0)

                # get application logs from history server
                url = "http://{}:18080/api/v1/applications/{}/logs".format(
                    master.address, expe["app_id"]
                )
                app_logs = None
                retry = 0
                while app_logs == None and retry < 3:
                    try:
                        app_logs = urllib2.urlopen(url)
                    except:
                        time.sleep(5)
                        retry = retry + 1
                filename = "{result_dir}/spark_job_{job_id}_events.zip".format(
                    result_dir=result_dir, job_id=expe["job_id"])
                with open(filename, "wb") as myfile:
                    myfile.write(app_logs.read())

                # TODO also store containers and executors informations

                df = DataFrame(expe, index=[expe["job_id"]])
                result_df = result_df.append(df)

                # restore the preempted resources
                if expe["preemption"] != "none":
                    cmd = (
                        'su - oar -c \''
                        '{path}/restore_resources.sh '
                        '"{node_list}" "{result_dir}"\''.format(
                            path=script_path,
                            preemption_type=expe["preemption"],
                            node_list=node_list,
                            result_dir=result_dir,
                            time=time_to_wait))
                    restore = SshProcess(
                        cmd,
                        master,
                        connection_params={"user": "root"})
                    restore.stdout_handlers.append(
                        '{}/restore_after_job_{}.out'.format(result_dir, job_id))
                    logger.info("Restore resources to Yarn")
                    restore.run()

            logger.info("Expe Finished: {}".format(expe))
            self.sweeper.done(expe)

            # Store application information
            #gather_bigdata_jobs_info(master, result_dir)
            result_df.to_csv(results_filepath)

        logger.info("results are stored in: {}".format(result_dir))
            #finally:
            #    if is_a_test:
            #        ipdb.set_trace()
            #    if oar_job_id is None:
            #        logger.info("delete job: {}".format(oar_job_id))
            #        oardel([(oar_job_id, site)])


if __name__ == "__main__":
    #import signal
    #signal.signal(signal.SIGINT, lambda *args: ipdb.set_trace())
    engine = BeBiDaExperiment()
    expe_tools.script_path =script_path
    engine.start()
