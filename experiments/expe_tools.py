from __future__ import print_function
import os
import time
import json
import errno
from shutil import copy

from execo import Process, SshProcess, Remote, format_date, \
    Put, Report, logger
from execo_g5k import oarsub, OarSubmission, get_oar_job_nodes, \
    wait_oar_job_start
from execo_g5k.kadeploy import deploy, Deployment


script_path = None


def get_resources(resources, walltime, site, job_type='deploy', oar_job_id=None, options=None):
    if oar_job_id is not None:
        logger.info("Using already sumitted OAR job: {}".format(oar_job_id))
        jobs = [(oar_job_id, site)]
    else:
        jobs = oarsub([(OarSubmission(resources=resources,
                                      job_type=job_type,
                                      walltime=walltime,
                                      additional_options=options)
                        , site)])
    job_id, site = jobs[0]

    logger.info("waiting job start {} on {}".format(job_id, site))
    wait_oar_job_start(
        job_id, site, prediction_callback=prediction_callback)

    logger.info("getting nodes of {} on {}".format(job_id, site))
    nodes = get_oar_job_nodes(job_id, site)

    # sort the nodes
    nodes = sorted(nodes, key=lambda node: node.address)

    return job_id, nodes


def deploy_images(master, master_env, slaves, slave_env,
                  force_redeploy=None,
                  required_resource_nb=None):
    # Force the node to be redeployed even if it was already deployed
    # to force clean state
    # FIXME this actually force the deployement and then raise deployement
    # failed error. As a workaround it can be run once and then rerun the
    # entire script without the option
    if force_redeploy == "all":
        check_deployed_command_slaves = False
        check_deployed_command_master = False
    elif force_redeploy == "master":
        check_deployed_command_slaves = True
        check_deployed_command_master = False
    elif force_redeploy == "slaves":
        check_deployed_command_slaves = False
        check_deployed_command_master = True
    else:
        check_deployed_command_slaves = True
        check_deployed_command_master = True

    if required_resource_nb is not None:
        def to_check(ok, ko):
            return len(ok) > required_resource_nb
    else:
        def to_check(ok, ko):
            return None
    # TODO do deploy in parallel
    logger.info("deploying slave nodes: {}".format(str(slaves)))
    deployed, undeployed = deploy(
        Deployment(slaves, env_name=slave_env),
        check_deployed_command=check_deployed_command_slaves,
        check_enough_func=to_check)
    if undeployed:
        logger.warn(
            "NOT deployed nodes: {}".format(str(undeployed)))
        raise RuntimeError('Deployement failed')

    if not master:
        return
    logger.info("deploying master nodes: {}".format(str(master)))
    deployed, undeployed = deploy(
        Deployment([master], env_name=master_env),
        check_deployed_command=check_deployed_command_master,
        check_enough_func=to_check)
    if undeployed:
        logger.warn(
            "NOT deployed nodes: {}".format(str(undeployed)))
        raise RuntimeError('Deployement failed')


def configure_OAR(tmp_dir, result_dir, master, slaves, nodes_info):
    # Configure OAR

    # Change log directory and disable (buggy) sentinelle script
    configure_oar_cmd = """
    sed -i \
        -e 's#^\(LOG_FILE\)\=.*#\\1="{}/oar.log"#' \
        -e 's#^\(LOG_LEVEL\)\="2"#\\1\="3"#' \
        -e 's#^\(PINGCHECKER_SENTINELLE_SCRIPT_COMMAND.*\)#\\1="/bin/true"#' \
        /etc/oar/oar.conf
    """.format(result_dir)
    configure_oar = Remote(configure_oar_cmd, [master],
                           connection_params={'user': 'root'})
    configure_oar.run()
    logger.info("OAR logger configured on master")

    # propagate SSH keys
    logger.info("configuring OAR SSH")
    oar_ssh_dir = tmp_dir + "/.ssh"

    # Get key from master
    Process('scp -o BatchMode=yes -o PasswordAuthentication=no '
            '-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null '
            '-o ConnectTimeout=20 -rp -o User=root ' +
            master.address + ":/var/lib/oar/.ssh"
            ' ' + oar_ssh_dir).run()
    # Push the key to the slaves
    Put(slaves, [oar_ssh_dir], "/var/lib/oar/",
        connection_params={'user': 'root'}).run()

    # Add resources to OAR
    logger.info("register resources")
    add_resources_cmd = """
    oarproperty -a core || true; \
    oarproperty -c -a host || true; \
    oarproperty -a mem || true; \
    """
    # List resources
    res_id = 1
    for node in slaves:
        node_info = [n for n in nodes_info if n["address"] == node.address][0]
        node_info["core_per_nodes"] = int(node_info["nb_proc"])
        for procs in range(node_info["core_per_nodes"]):
            node_info["res_id"] = res_id
            # Put a test in the resource add to only add resource if necessary:
            # One resource per core
            add_resources_cmd = (
                add_resources_cmd +
                "test {core_per_nodes} -eq "
                "$(oarnodes | grep \"network_address : {address}\" | wc -l) || "
                "oarnodesetting -a -h {address} -p host={address} "
                "-p core={res_id} "
                "-p cpuset=0 -p mem={memory}; \\\n"
            ).format(**node_info)
            res_id = res_id + 1
    # Do add resources
    add_resources = SshProcess(add_resources_cmd + "exit 0", master,
                               connection_params={'user': 'root'})
    add_resources.run()

    if add_resources.ok:
        logger.info("oar is now configured!")
    else:
        logger.error("oar is NOT configured:\n{}".format(
            Report([add_resources])))
        raise RuntimeError("error in the OAR configuration: Abort!")


def activate_OAR_Prologue_Epilogue(master, slaves):

    # Change log directory and enable epilogue prologue
    configure_oar_cmd = """
    sed -i \
    -e 's#^\#\(SERVER_PROLOGUE_EPILOGUE_TIMEOUT\)\=.*#\\1="60"#' \
    -e 's#^\#\(SERVER_PROLOGUE_EXEC_FILE\)\=.*#\\1="/etc/oar/server_prologue"#' \
    -e 's#^\#\(SERVER_EPILOGUE_EXEC_FILE\)\=.*#\\1="/etc/oar/server_epilogue"#' \
    /etc/oar/oar.conf
    """
    configure_oar = Remote(configure_oar_cmd, [master],
                           connection_params={'user': 'root'})
    configure_oar.run()
    logger.info("OAR serveur pro/epilogue is activated")

    # import and set prologue and epilogue
    epilogue = script_path + "/config/server_epilogue"
    prologue = script_path + "/config/server_prologue"
    Put([master], [prologue, epilogue], "/etc/oar/",
        connection_params={'user': 'root'}).run()

    # import Hadoop auxiliary script for pro/epilogue
    aux_scripts = [script_path + "/config/hadoop_nm_start.sh",
                   script_path + "/config/hadoop_nm_stop.sh",
                   script_path + "/config/wait_for_nm.sh"]
    Put([master], aux_scripts, "/opt/hadoop/etc/hadoop/",
        connection_params={'user': 'root'}).run()

    # set the permission on prologue/epilogue files
    SshProcess("chmod 755 /etc/oar/server_*", master,
               connection_params={'user': 'root'}).run()

    # install pyyaml to make the epilogue/prologue works
    SshProcess("apt install python-yaml", master,
               connection_params={'user': 'root'}).run()

    # add OAR to spark group
    Remote("adduser oar spark", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add OAR to hadoop group
    Remote("adduser oar hadoop", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add authorization for OAR to act as hadoop using passwordless su
    auth_cmd = """\
sed -i '/auth       sufficient pam_rootok.so/a\
auth       [success=ignore default=1] pam_succeed_if.so user = hadoop\\n\
auth       sufficient   pam_succeed_if.so use_uid user = oar' \
/etc/pam.d/su"""

    Remote(auth_cmd, [master] + slaves,
           connection_params={'user': 'root'}).run()


def configure_SLURM(tmp_dir, result_dir, master, slaves, nodes_info):
    """
    FIXME: not tested and thus not working.
    """
    logger.info("Prepare SLURM configuration files ("+result_dir+"/slurm.conf and others)")

    #copy them to the result_dir
    Process("cp {}/../expe_files/slurm* {}/".format(script_path, result_dir), shell=True).run()

    # change directories
    Process("""/bin/bash -c "\
    sed -i 's|=/tmp|={result_dir}/|g' {result_dir}/slurmdbd.conf ; \
    sed -i 's|CONTROL_MACHINE_SHORT|{addr}|g' {result_dir}/slurm.conf ; \
    sed -i 's|LOGS_PATH|{result_dir}|g' {result_dir}/slurm.conf ; \
    " """.format(result_dir=result_dir, addr=master.address.split('.')[0], shell=True)).run()
    
    #Node cfg
    add_resources_str = ""
    for node in slaves:
        node_info = [n for n in nodes_info if n["address"] == node.address][0]
        add_resources_str = (
            add_resources_str + """
            NodeName={address} CPUs={nb_proc} NodeAddr={address} RealMemory={memory} Port=6821"""
        ).format(**node_info)
    
    add_resources_str = add_resources_str+"""
    PartitionName=debug Nodes="""+",".join([n.address for n in slaves])+""" Default=YES MaxTime=INFINITE State=UP
    """
    add_resources_cmd = "echo '{}' >>  {}/slurm.conf".format(add_resources_str, result_dir)

    Process(add_resources_cmd, shell=True).run()
    
    
    logger.info("Deploy SLURM config files")
    Put(slaves+[master],
        [
            result_dir+"/slurm.cert",
            result_dir+"/slurm.conf",
            result_dir+"/slurmdbd.conf",
            result_dir+"/slurm.key"],
        "/etc/slurm-llnl/",
        connection_params={'user': 'root'}).run()
    
    logger.info("Start SLURM daemons on master")
    # my experience is to always wait a bit after each command.
    master_cmd = SshProcess(
        """slurmdbd ; \
        sleep 1; \
        sacctmgr -i add cluster cluster ; \
        sleep 1 ; \
        slurmctld ; \
        sleep 1 \
        """, master, connection_params={'user': 'root'})
    master_cmd.run()
    
    logger.info("Start SLURM daemons on slaves")
    slaves_cmd = Remote("slurmd ; sleep 1 ", slaves, connection_params={'user': 'root'})
    slaves_cmd.run()
    
    logger.info("SLURM configurated and started.")


def activate_SLURM_Prologue_Epilogue(tmp_dir, result_dir, master, slaves, nodes_info):
    """
    FIXME: not tested and thus not working.
    """
    
    # prepare slurm translators
    Process("""/bin/bash -c "\
    cp {script_path}/config/slurm2oar_logue.sh {tmp_dir}/slurm2oar_prologue ; \
    cp {script_path}/config/slurm2oar_logue.sh {tmp_dir}/slurm2oar_epilogue ; \
    sed -i -e \'s|=ASCRIPTTOCALL|/etc/slurm-llnl/server_prologue|g' {tmp_dir}/slurm2oar_prologue ; \
    sed -i -e \'s|=ASCRIPTTOCALL|/etc/slurm-llnl/server_epilogue|g' {tmp_dir}/slurm2oar_epilogue ; \
    " """.format(script_path=script_path, tmp_dir=tmp_dir)).run()

    # configure slurm
    configure_slurm_cmd = """\
    echo '
#PrologEpilogTimeout=30
PrologSlurmctld=/etc/slurm-llnl/slurm2oar_prologue
EpilogSlurmctld=/etc/slurm-llnl/slurm2oar_epilogue
    ' >> /etc/slurm-llnl/slurm.conf ; \
    scontrol reconfigure
    """
    configure_oar = Remote(configure_slurm_cmd, [master]+slaves,
                           connection_params={'user': 'root'})
    configure_oar.run()
    logger.info("SLURM serveur pro/epilogue is activated")


    # import and set prologue and epilogue
    epilogue = script_path + "/config/server_epilogue"
    prologue = script_path + "/config/server_prologue"
    translators = [tmp_dir+"/slurm2oar_prologue", tmp_dir+"/slurm2oar_epilogue"]
    Put([master], [prologue, epilogue]+translators, "/etc/slurm-llnl/",
        connection_params={'user': 'root'}).run()

    # import Hadoop auxiliary script for pro/epilogue
    aux_scripts = [script_path + "/config/hadoop_nm_start.sh",
                   script_path + "/config/hadoop_nm_stop.sh",
                   script_path + "/config/wait_for_nm.sh"]
    Put([master], aux_scripts, "/opt/hadoop/etc/hadoop/",
        connection_params={'user': 'root'}).run()

    # set the permission on prologue/epilogue files
    SshProcess("chmod 755 /etc/slurm-llnl/*", master,
               connection_params={'user': 'root'}).run()

    # install pyyaml to make the epilogue/prologue works
    SshProcess("apt install python-yaml", master,
               connection_params={'user': 'root'}).run()

    # add SLURM to spark group
    Remote("adduser slurm spark", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add SLURM to hadoop group
    Remote("adduser slurm hadoop", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add authorization for SLURM to act as hadoop using passwordless su
    auth_cmd = """\
sed -i '/auth       sufficient pam_rootok.so/a\
auth       [success=ignore default=1] pam_succeed_if.so user = hadoop\\n\
auth       sufficient   pam_succeed_if.so use_uid user = slurm' \
/etc/pam.d/su"""

    Remote(auth_cmd, [master] + slaves,
           connection_params={'user': 'root'}).run()


def configure_SSH(tmp_dir, master, slaves, user="root"):
    # Configure SSH for the provided user
    home_dir = SshProcess("eval echo ~" + user, master).run().stdout.strip()
    logger.info("enable master SSH connection to slaves and himself")

    # generate key
    ssh_key = tmp_dir + "/id_rsa"
    Process('rm -rf ' + ssh_key).run()
    Process("ssh-keygen -P '' -f {}".format(ssh_key)).run()

    # create ssh dir on master if needed
    SshProcess("mkdir -p {home}/.ssh".format(home=home_dir), master,
               connection_params={'user': 'root'}).run()

    ssh_config_content = """
Host *
    ForwardX11 no
    StrictHostKeyChecking no
    PasswordAuthentication no
    AddressFamily inet
    UserKnownHostsFile=/dev/null
"""

    ssh_config_file = tmp_dir + '/config'
    with open(ssh_config_file, 'w') as text_file:
        text_file.write(ssh_config_content)

    # push master ssh config and private key
    private_key = Put([master], [ssh_config_file, ssh_key],
                      "{home}/.ssh/".format(home=home_dir),
                      connection_params={'user': 'root'})
    private_key.run()

    # push public key everywhere and fix permission
    Put([master] + slaves, [ssh_key + ".pub"], "{home}/id_rsa.pub".format(home=home_dir),
        connection_params={'user': 'root'}).run()
    cmd = "mkdir -p {home}/.ssh && \
           cat {home}/id_rsa.pub >> {home}/.ssh/authorized_keys && \
           chown -R {user}:{user} {home}/.ssh && \
           chmod 700 {home}/.ssh && chmod 600 {home}/.ssh/authorized_keys \
          ".format(home=home_dir, user=user)
    public_key = Remote(cmd, [master] + slaves,
                        connection_params={'user': 'root'})
    public_key.run()

    if public_key.ok and private_key.ok:
        logger.info("SSH is now configured!")
    else:
        logger.error("SSH is NOT configured:\n{}".format(
            Report([public_key, private_key])))
        raise RuntimeError("error in the SSH configuration: Abort!")


def configure_Hadoop(tmp_dir, master, slaves, nodes_info,
                     data_dir="/tmp/hdfs_root",
                     replication=3):
    # FIXME: this will not work with a home on /home because of the NFS: put it
    # on /opt or something
    # get ssh access
    logger.info("Configure SSH for hadoop user")
    configure_SSH(tmp_dir, master, slaves, user="hadoop")

    # Configure slaves
    logger.info("configure Hadoop slaves")

    # Configure DataNodes

    # Add DataNodes list in $HADOOP_HOME/etc/hadoop/slaves
    slaves_list = tmp_dir + "/slaves"
    with open(slaves_list, "w") as slave_file:
        for slave in slaves:
            slave_file.write("{}\n".format(slave.address))

        logger.info("configure HDFS")
    # Generate core site config
    site_config_header = """<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>"""

    core_site_config = site_config_header + """
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://{}:9000</value>
    </property>
</configuration>
""".format(master.address)

    core_site_config_file = tmp_dir + '/core-site.xml'
    with open(core_site_config_file, 'w') as text_file:
        text_file.write(core_site_config)

    logger.info("configure YARN")
    # Generate core site config
    yarn_config_header = """<?xml version="1.0" encoding="UTF-8"?>"""

    # Set only one container per node (considering master node is similar to
    # the compute node).
    nb_cores = int(nodes_info[0]["nb_proc"])
    # get it in megabytes and keep 512MB for the system
    memory = (int(nodes_info[0]["memory"]) / 1024) - 512
    yarn_site_config = yarn_config_header + """
<configuration>
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>{master}</value>
    </property>
    <property>
        <name>yarn.nodemanager.health-checker.interval-ms</name>
        <value>30000</value>
    </property>
    <property>
        <name>yarn.resourcemanager.nodes.exclude-path</name>
        <value>/opt/hadoop/etc/hadoop/yarn.exclude</value>
        <final>true</final>
    </property>
    <property>
        <name>yarn.nodemanager.resource.cpu-vcores</name>
        <value>{nodemanager_nbcores}</value>
    </property>
    <property>
        <name>yarn.nodemanager.resource.memory-mb</name>
        <value>{nodemanager_memory}</value>
    </property>
    <property>
        <name>yarn.scheduler.minimum-allocation-vcores</name>
        <value>1</value>
    </property>
    <property>
        <name>yarn.scheduler.maximum-allocation-vcores</name>
        <value>{max_core_alloc}</value>
    </property>
    <property>
        <name>yarn.scheduler.minimum-allocation-mb</name>
        <value>1024</value>
    </property>
    <property>
        <name>yarn.scheduler.maximum-allocation-mb</name>
        <value>{max_mb_alloc}</value>
    </property>
    <property>
        <name>yarn.resourcemanager.scheduler.class</name>
        <value>org.apache.hadoop.yarn.server.resourcemanager.scheduler.capacity.CapacityScheduler</value>
    </property>
    <property>
        <name>yarn.nodemanager.disk-health-checker.enable</name>
        <value>false</value>
    </property>
    <property>
        <name>yarn.node-labels.enabled</name>
        <value>true</value>
    </property>
    <property>
        <name>yarn.node-labels.fs-store.root-dir</name>
        <value>file:///opt/hadoop/etc/hadoop/node-label</value>
    </property>
</configuration>
""".format(master=master.address,
           nodemanager_nbcores=nb_cores,
           nodemanager_memory=memory,
           container_vcores=nb_cores,
           container_memory=memory,
           max_mb_alloc=memory,
           max_core_alloc=nb_cores)

    yarn_site_config_file = tmp_dir + '/yarn-site.xml'
    with open(yarn_site_config_file, 'w') as text_file:
        text_file.write(yarn_site_config)

    # Get HDFS config from ./config folder
    hdfs_site_config = yarn_config_header + """
    <configuration>
        <property>
            <name>dfs.replication</name>
            <value>{replication}</value>
        </property>
        <property>
            <name>dfs.data.dir</name>
            <value>{data_dir}</value>
        </property>
        <property>
            <name>dfs.datanode.du.reserved</name>
            <value>{reserved_hdfs}</value>
        </property>
    </configuration>
    """.format(
        replication=replication,
        data_dir=data_dir,
        reserved_hdfs=1024**3 * 25)
    # FIXME the reserved_hdfs value is here set to 5Gb to avoid health check of
    # yarn to exculde node because of HDFS taking all the storage space. It
    # would be better to set this to 0.25 * the disk size. This is due to the
    # fact that logs and local cache dir are one the same block as HDFS

    hdfs_site_config_file = tmp_dir + '/hdfs-site.xml'
    with open(hdfs_site_config_file, 'w') as text_file:
        text_file.write(hdfs_site_config)

    # get capacity sched config from a file
    capacity_sched_config_file = script_path + '/config/capacity-scheduler.xml'

    Put([master] + slaves,
        [slaves_list,
         hdfs_site_config_file,
         core_site_config_file,
         yarn_site_config_file,
         capacity_sched_config_file],
        "/opt/hadoop/etc/hadoop/",
        connection_params={'user': 'root'}
        ).run()

    # set the permission on yarn exclude files
    SshProcess("chmod 664 /opt/hadoop/etc/hadoop/yarn.exclude", master,
               connection_params={'user': 'root'}).run()

def add_label_to_nodes(master, node_label_map):
    node_label_list = " ".join(
        [node.address + "=" + label for node, label in node_label_map.items()])
    logger.info("Add labels to nodes: {}".format(node_label_list))
    cmd = ('su - hadoop -c "'
           'yarn rmadmin -addToClusterNodeLabels \\"{labels}\\";'
           'yarn rmadmin -replaceLabelsOnNode \\"{node_label_list}\\";"'
          ).format(labels=','.join(node_label_map.values()),
                   node_label_list=node_label_list)
    label_proc = SshProcess(cmd, master, connection_params={'user': 'root'},
                            shell=True)
    label_proc.run()
    if label_proc.ok:
        logger.info("Labels added")
    else:
        logger.error("labels is NOT added:\n{}".format(
            Report([label_proc])))
        raise RuntimeError("error while adding labels: Abort!")


    logger.info(label_proc.stdout + label_proc.stderr)
    return label_proc


def configure_Spark(tmp_dir, master, slaves, nodes_info, location="HDFS",
                   nb_executor_per_nodes=2, enable_labels=False):
    logger.info("Configuring Spark")
    # Add slave list in $SPARK_HOME/conf/slaves
    slaves_list = tmp_dir + "/slaves"
    with open(slaves_list, "w") as slave_file:
        for slave in slaves:
            slave_file.write("{}\n".format(slave.address))

    # Configure eventLog directory
    create_HDFS_user_home(master, 'spark')

    # Use this for HDFS
    if location == "HDFS":
        event_log_dir = "/user/spark/applicationHistory"
        event_log_url = "hdfs://{master_addr}:9000{event_log_dir}".format(
            master_addr=master.address, event_log_dir=event_log_dir)
        cmd = ("$HADOOP_HOME/bin/hdfs dfs -mkdir -p {event_log_dir};"
               "$HADOOP_HOME/bin/hdfs dfs -chown spark:spark {event_log_dir};"
               "$HADOOP_HOME/bin/hdfs dfs -chmod 1777 {event_log_dir};"
               ).format(event_log_dir=event_log_dir)

    # Use this for local
    else:
        event_log_dir = "/tmp/spark-events"
        event_log_url = "file://" + event_log_dir
        cmd = ("mkdir -p {event_log_dir};"
               "chown spark:spark {event_log_dir};"
               "chmod 777 {event_log_dir};"
               ).format(event_log_dir=event_log_dir)

    config_event_log = SshProcess(
        cmd, master,
        connection_params={'user': 'root'}, shell=True)
    config_event_log.run()

    # Set default spark config
    # Cores and memory is computed using node memory and cores (making the
    # assumption that the nodes are homogeneous)
    nb_cores = int(nodes_info[0]["nb_proc"]) / nb_executor_per_nodes
    # Leave 512Mb for the system and take into acount the spark ovehead 10% of
    # the memory
    memory_GB = int((((int(nodes_info[0]["memory"]) / 1024) - 512) * 0.9 /
                 nb_executor_per_nodes) // 1024)

    # To enable dynamica allocation:
    # spark.dynamicAllocation.enabled  true
    # spark.shuffle.service.enabled    true
    # in standalone mode use
    # spark.master                     spark://{master_addr}:7077
    spark_config = """
spark.eventLog.enabled           true
spark.eventLog.dir               {event_log_url}
spark.history.fs.logDirectory    {event_log_url}
spark.yarn.historyServer.address {master_addr}:18080
spark.task.maxFailures           250
spark.yarn.max.executor.failures 250
spark.yarn.am.memory             1g
spark.yarn.am.cores              1
spark.executor.instances         {nb_executor}
spark.executor.memory            {executor_memory}g
spark.executor.cores             {executor_cores}
spark.driver.memory              1g
""".format(event_log_url=event_log_url,
           master_addr=master.address,
           nb_executor=len(slaves) * nb_executor_per_nodes,
           executor_cores=nb_cores,
           executor_memory=memory_GB)
    if enable_labels:
        spark_config = spark_config + """
spark.yarn.am.nodeLabelExpression       AM_only
spark.yarn.executor.nodeLabelExpression  executor_only
"""
    spark_config_file = tmp_dir + '/spark-defaults.conf'
    with open(spark_config_file, 'w') as text_file:
        text_file.write(spark_config)

    # Push the files to the nodes
    Put([master] + slaves,
        [slaves_list, spark_config_file],
        "/opt/spark/conf/",
        connection_params={'user': 'root'}
        ).run()

    logger.info("Spark is now configured")


def format_HDFS(master, slaves, force=False):
    """ format HDFS filesystem (this is requiered to start HDFS)"""
    logger.info("format HDFS")

    if force:
        logger.info("Delete hdfs root from nodes")
        Remote("rm -rf /tmp/hdfs_root /tmp/hadoop-root", [master] + slaves, connection_params={'user': 'root'}).run()
    format_cmd = "$HADOOP_HOME/bin/hdfs namenode -format -nonInteractive"

    if force:
        logger.warn("format will be FORCED, all data will be erased!")
        format_cmd += " -force"

    format_hdfs = SshProcess(format_cmd,
                             master,
                             connection_params={'user': 'root'},
                             shell=True)
    format_hdfs.run()
    if format_hdfs.ok:
        logger.info("HDFS is now formatted")
    else:
        logger.error("HDFS is NOT formatted")
        raise RuntimeError("error in the HDFS format process: Abort!")


def start_HDFS(master):
    """ start HDFS namenode and datanodes"""
    logger.info("start HDFS daemon")
    start_hdfs = SshProcess("$HADOOP_HOME/sbin/start-dfs.sh; $HADOOP_HOME/bin/hdfs dfsadmin -safemode leave", master,
                            connection_params={'user': 'root'}, shell=True)
    start_hdfs.run()
    logger.info('HDFS succesfully started '
                'WebUI:\nhttp://{}:50070'.format(master.address))


def stop_HDFS(master):
    """ stop HDFS namenode and datanodes"""
    logger.info("stop HDFS daemon")
    stop_hdfs = SshProcess("$HADOOP_HOME/sbin/stop-dfs.sh", master,
                           connection_params={'user': 'root'}, shell=True)
    stop_hdfs.run()


def start_YARN(master, slaves):
    """ start YARN from the master (it also starts the slaves if defined"""
    logger.info("start YARN daemons")
    start_yarn = SshProcess(
        'su - hadoop -c "$HADOOP_HOME/sbin/start-yarn.sh"',
        master,
        connection_params={'user': 'root'}, shell=True)
    start_yarn.run()

    # Remote('su - hadoop -c "$HADOOP_HOME/sbin/yarn-daemon.sh start nodemanager"',
    #       slaves,
    #       connection_params={'user': 'root'}).run()

    logger.info('Hadoop YARN '
                'WebUI:\nhttp://{}:8088'.format(master.address))
    return start_yarn


def stop_YARN(master, slaves):
    """ stop YARN from the master (it also stops the slaves if defined"""
    logger.info("stop YARN daemons")
    stop_yarn = SshProcess(
        'su - hadoop -c "$HADOOP_HOME/sbin/yarn-daemon.sh stop resourcemanager"',
        master,
        connection_params={'user': 'root'}, shell=True)
    stop_yarn.run()

    Remote('su - hadoop -c "$HADOOP_HOME/sbin/yarn-daemon.sh stop nodemanager"',
           slaves,
           connection_params={'user': 'root'}).run()


def create_HDFS_user_home(master, username):
    """ create a home in HDFS for a user to allows him to write in HDFS """
    home_dir = "/user/" + username
    logger.info("create home directory: " + home_dir)
    cmd = "$HADOOP_HOME/bin/hadoop fs -mkdir -p " + home_dir

    create_home = SshProcess(cmd, master,
                             connection_params={'user': 'root'}, shell=True)
    create_home.run()

    cmd = "$HADOOP_HOME/bin/hadoop fs -chown " + username + " " + home_dir

    set_acl_home = SshProcess(
        cmd, master,
        connection_params={'user': 'root'}, shell=True)
    set_acl_home.run()
    return home_dir


def start_Spark_standalone(master, slaves):
    # Start Spark master
    logger.info("start Spark master daemon")
    start_spark_master = SshProcess("$SPARK_HOME/sbin/start-master.sh",
                                    master,
                                    connection_params={'user': 'root'},
                                    shell=True)
    start_spark_master.run()
    logger.info('Spark '
                'WebUI:\nhttp://{}:8080'.format(master.address))

    # Start Spark slaves
    logger.info("start Spark slaves daemon")
    start_spark_slaves = Remote(
        "sudo -u oar $SPARK_HOME/sbin/start-slave.sh spark://{}:7077".format(
            master.address),
        slaves,
        connection_params={'user': 'root'})
    start_spark_slaves.run()

    start_Spark_history_server(master)


def stop_Spark_history_server(master):
    # Start Spark history server
    logger.info("stop Spark history server daemon")
    stop_spark_hist = SshProcess("$SPARK_HOME/sbin/stop-history-server.sh",
                                 master,
                                 connection_params={'user': 'root'},
                                 shell=True)
    stop_spark_hist.run()


def start_Spark_history_server(master):
    # Start Spark history server
    logger.info("start Spark history server daemon")
    start_spark_hist = SshProcess("$SPARK_HOME/sbin/start-history-server.sh",
                                  master,
                                  connection_params={'user': 'root'},
                                  shell=True)
    start_spark_hist.run()

    logger.info('Spark History server '
                'WebUI:\nhttp://{}:18080'.format(master.address))


def generate_data(node, data_size_GB, app_type, data_store="/tmp",
                  bench_home="/opt/BigDataBench_V3.2.5_Spark"):
    dataset = "data-{app_type}-{size}".format(
        size=data_size_GB, app_type=app_type)

    # do not regenerate data when already in the data_store
    check_cmd = "test -e {}/{}".format(data_store, dataset)
    check = SshProcess(check_cmd, node).run()
    if check.ok:

        logger.info("Dataset of type {} and size {}GB is already present".format(
            app_type, data_size_GB))
        return

    logger.info("Generating a dataset of type {} and size "
                "{}GB...".format(app_type, data_size_GB))
    # Split it because it is not generic
    if app_type == "MicroBenchmarks":

        # replace default generation script
        gen_script = script_path + "/config/genData_MicroBenchmarks.sh"
        Put([node], [gen_script], "{}/MicroBenchmarks".format(bench_home),
            connection_params={'user': 'root'}).run()
        cmd = """
cd {bench_home}/MicroBenchmarks;
./genData_MicroBenchmarks.sh {data_store} {dataset} <<EOF
{size}
EOF""".format(size=data_size_GB,
              bench_home=bench_home,
              dataset=dataset,
              data_store=data_store)

        ret = SshProcess(cmd, node)

    elif app_type == "SNS-Kmeans":

        # replace default generation script
        gen_script = "{}/config/genData_Kmeans.sh".format(script_path)
        Put([node], [gen_script], bench_home + "/SNS/Kmeans/",
            connection_params={'user': 'root'}).run()
        cmd = """
cd {bench_home}/SNS/Kmeans;
./genData_Kmeans.sh {data_store} {dataset} <<EOF
{size}
EOF""".format(size=data_size_GB,
              bench_home=bench_home,
              dataset=dataset,
              data_store=data_store)

        ret = SshProcess(cmd, node)
    else:
        raise AttributeError("Unknown Application type:" + app_type)
    return ret.start()


def gather_nodes_information(nodes, result_dir):
    # Do this in reverse because master node can still be booting
    info_gather = Remote("nproc; awk '/MemTotal/ {print $2}' /proc/meminfo",
                         reversed(nodes))
    info_gather.run()
    nodes_info = []
    for process in info_gather.processes:
        nodes_info.append({"address": process.host.address,
                           "nb_proc": process.stdout.split('\n')[0].strip(),
                           "memory": process.stdout.split('\n')[1].strip()})

    with open(result_dir + "/nodes_info.json", "w+") as nodes_file:
        json.dump(nodes_info, nodes_file)

    return nodes_info


def prediction_callback(ts):
    logger.info("job start prediction = {}".format(format_date(ts)))


def setup_result_dir(is_a_test=False):

    if is_a_test:
        result_dir = "test_results"
    else:
        result_dir = "results"
    try:
        os.mkdir(result_dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
    result_path = \
        script_path + '/' + result_dir + '/results_' + \
        time.strftime("%Y-%m-%d--%H-%M-%S")

    return result_path


def load_experiment_config(config_file, result_dir):
    # import configuration
    with open(config_file) as config_file_des:
        config = json.load(config_file_des)

    # backup configuration
    copy(config_file, result_dir)
    return config


def remove_HDFS_files(master, path):
    cmd = "$HADOOP_HOME/bin/hdfs dfs -rm -r -f {}".format(path)

    rm_resutl_dir = SshProcess(cmd, master,  shell=True)
    rm_resutl_dir.run()


def start_BigDataBench_application(expe, master, result_dir, job_id):
    # Remove results dir if necessary
    results_subdir = "{application_name}-{iteration}-result{job_id}".format(
        **expe)
    remove_HDFS_files(master, results_subdir)

    # expand parameters
    expe["application_parameters"] = expe[
        "application_parameters"].format(**expe)

    # Submit a Spark job command
    spark_submit_cmd = """/opt/spark/bin/spark-submit \
    --master yarn \
    --deploy-mode client \
    --class cn.ac.ict.bigdatabench.{application_name} \
    /opt/BigDataBench_V3.2.5_Spark/JAR_FILE/bigdatabench-spark_1.3.0-hadoop_1.0.4.jar \
    /user/{user}/data-{application_type}-{data_size} {application_parameters}""".format(**expe)

    # Start spark app
    logger.info("Start Spark application {}...")
    job = SshProcess(spark_submit_cmd, master)
    job.stdout_handlers.append(result_dir +
                               '/spark_job_{}.out'.format(job_id))
    job.stderr_handlers.append(result_dir +
                               '/spark_job_{}.err'.format(job_id))
    logger.info("Spark jobs {} started".format(job_id))
    return job.start()


def push_dataset_to_HDFS(node, profile, data_store, hdfs_home):
    dataset = "data-{app_type}-{size}".format(
        size=profile["data_size"],
        app_type=profile["application_type"])

    # check if the dataset exists in HDFS
    check_cmd = (
        "$HADOOP_HOME/bin/hdfs dfs -test -e {hdfs_home}/{dataset} && "
        "test $($HADOOP_HOME/bin/hdfs dfs -du -s {hdfs_home}/{dataset} | cut -f1 -d' ') "
        " -le "
        " $(du -b -s {data_store}/{dataset} | cut -f1)".format(
            hdfs_home=hdfs_home, dataset=dataset, data_store=data_store)
    )

    check = SshProcess(check_cmd, node).run()
    if check.ok:
        logger.info("Dataset {} is already in HDFS".format(dataset))
        return

    logger.info("Let's push {} dataset to HDFS...".format(dataset))

    push_cmd = "$HADOOP_HOME/bin/hadoop distcp -m {max_process} -update \
            file://{data_store}/{dataset} hdfs://{hdfs_home}/{dataset}".format(
        dataset=dataset,
        data_store=data_store,
        hdfs_home=hdfs_home,
        max_process=50)
    return SshProcess(push_cmd, node).start()


def mount_storage5k(nodes, data_dir):
    """ Mount the data directory on the defined nodes """
    # TODO check if already mounted to avoid error message
    logger.info("mounting storage5k directory on nodes: {}".format(nodes))
    cmd = "mkdir -p {data_store} && mount -t nfs4 -o rw,relatime,vers=4.0,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,port=0,timeo=600,retrans=2,sec=sys,local_lock=none storage5k.nancy.grid5000.fr:{data_store} {data_store}".format(
        data_store=data_dir)
    Remote(cmd, nodes, connection_params={'user': 'root'}).run()
