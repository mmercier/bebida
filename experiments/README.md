# Run the Notebooks

In almost each of the experiment folder there is a notebook, in order to
run it on grid5000 just folow these steps:

## On grid5000

Take a machine with a lot of ram:
```sh
# localy
ssh -A grenoble.g5k
```

```sh
# Now on the frontend
oarsub -p "cluster='dahu'" -I -l "nodes=1,walltime=3:50:00"
```

Create a tunnel:
```sh
ssh -D 5000 -A grenoble.g5k
```

Then setup the sock proxy of sour browser to localhost:5000

```sh
# get root access
sudo-g5k

# Install nix
curl https://nixos.org/nix/install | sh

# start jupyter
export XDG_RUNTIME_DIR=~/.tmp

. /home/$USER/.nix-profile/etc/profile.d/nix.sh

nix-shell https://github.com/oar-team/kapack/archive/fe342846a8dfccc85077798357f9d7a9e889a8c7.tar.gz \
  -A evalysNotebookEnv --run \
  "jupyter-notebook --ip $(hostname)"
```

Click on the link that is given by jupyter


