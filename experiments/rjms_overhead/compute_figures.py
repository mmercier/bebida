#!/usr/bin/env python3
"""
This script generate figures about pro/epilogue overhead on RJMS system
It need a CSV file coming from the experiment that can be run like this::

    cd ../
    ./experiment.py ./rjms_overhead/overhead_expe_big.json

"""
import pandas as pd
import matplotlib.pyplot as plt


def create_figures(csv_file_path):
    df = pd.read_csv(csv_file_path,
                     sep=" ",
                     names=["type", "nb_nodes", "nb_running_jobs", "time"])

    fig, axes = plt.subplots(ncols=1, nrows=2)
    group_df = df.groupby(['type', 'nb_nodes', "nb_running_jobs"])['time']
    group_df.apply(pd.Series.reset_index)
    group_df.plot(ax=axes[0])
    axes[0].grid(True)
    axes[0].legend(ncol=2)
    group_df = df.groupby(['type'])['time']
    group_df.plot.hist(ax=axes[1],
                       alpha=0.5,
                       bins=20)
    axes[1].grid(True)
    axes[1].legend()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('CSV_file_path',
                        type=argparse.FileType('r'))
    args = parser.parse_args()
    create_figures(args.CSV_file_path)
    plt.show()
