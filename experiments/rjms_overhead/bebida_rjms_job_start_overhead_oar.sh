#
# BeBiDa project experiments about prologue and epilogue overhead on RJMS
# jobs start and stop
#
# Second part where we actually time the prologue and epilogue overhead
#
# WARNING: This must bu run as OAR user

set -x
set -e

nb_nodes=$1
result_dir=${2:-$PWD}
nb_running_jobs=$3

JOB_ID=$(cat "$result_dir/job_id")

for i in {1..20}
do
/usr/bin/time -f "prologue $nb_nodes $nb_running_jobs %e" -a -o \
        $result_dir/bebida_rjms_job_start_overhead.csv \
        /etc/oar/server_prologue $JOB_ID

sleep 5
/usr/bin/time -f "epilogue $nb_nodes $nb_running_jobs %e" -a -o \
        $result_dir/bebida_rjms_job_start_overhead.csv \
        /etc/oar/server_epilogue $JOB_ID
sleep 10

done
