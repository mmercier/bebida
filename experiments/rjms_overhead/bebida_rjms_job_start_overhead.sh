#
# BeBiDa project experiments about prologue and epilogue overhead on RJMS
# jobs start and stop
#
# First part where we prepare the cluster to be tested
#
# WARNING: This must bu run as normal user

set -x
set -e

nb_nodes=$1
result_dir=${2:-$PWD}

# clean previous runs
rm -f "$result_dir/done"

# run a job to have an usable epi/prologue
oarsub -l nodes=$nb_nodes \
  --notify "exec:/usr/bin/touch $result_dir/done" \
  -d $result_dir \
  'echo $OAR_JOBID > job_id; sleep 1'

# wait for it...
while [ ! -e "$result_dir/done" ]; do sleep 1; done
