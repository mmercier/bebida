#!/usr/bin/env python2

from __future__ import print_function
import os
import time
import json
import errno
from shutil import copy

from execo import Process, SshProcess, Remote, format_date, Put, Report
from execo_engine import Engine, logger
from execo_g5k import oarsub, oardel, OarSubmission, \
    get_oar_job_nodes, wait_oar_job_start
from execo_g5k.kadeploy import deploy, Deployment
from execo_engine import ParamSweeper, sweep

# For debug
import ipdb
import traceback

# For experiments result gathering
import urllib2
import re


script_path = os.path.dirname(os.path.realpath(__file__))


def get_resources(resources, walltime, site, oar_job_id=None):
    if oar_job_id is not None:
        jobs = [(oar_job_id, site)]
    else:
        jobs = oarsub([(OarSubmission(resources=resources,
                                      job_type='deploy',
                                      walltime=walltime), site)])
    job_id, site = jobs[0]

    logger.info("waiting job start {} on {}".format(job_id, site))
    wait_oar_job_start(
        job_id, site, prediction_callback=prediction_callback)

    logger.info("getting nodes of {} on {}".format(job_id, site))
    nodes = get_oar_job_nodes(job_id, site)

    # sort the nodes
    nodes = sorted(nodes, key=lambda node: node.address)

    return job_id, nodes


def deploy_images(master, master_env, slaves, slave_env,
                  force_redeploy=False):
    # Force the node to be redeployed even if it was already deployed
    # to force clean state
    if force_redeploy:
        check_deployed_command = "exit 1"
    else:
        check_deployed_command = True

    # TODO do deploy in parallel
    logger.info("deploying slave nodes: {}".format(str(slaves)))
    deployed, undeployed = deploy(
        Deployment(slaves, env_name=slave_env),
        check_deployed_command=check_deployed_command)
    if undeployed:
        logger.warn(
            "NOT deployed nodes: {}".format(str(undeployed)))
        raise RuntimeError('Deployement failed')

    logger.info("deploying master nodes: {}".format(str(master)))
    deployed, undeployed = deploy(
        Deployment([master], env_name=master_env),
        check_deployed_command=check_deployed_command)
    if undeployed:
        logger.warn(
            "NOT deployed nodes: {}".format(str(undeployed)))
        raise RuntimeError('Deployement failed')


def configure_OAR(tmp_dir, result_dir, master, slaves, nodes_info):
    # Configure OAR

    # Change log directory and enable epilogue prologue
    configure_oar_cmd = """
    sed -i \
        -e 's#^\(LOG_FILE\)\=.*#\\1="{}/oar.log"#' \
        /etc/oar/oar.conf
    """.format(result_dir)
    configure_oar = Remote(configure_oar_cmd, [master],
                           connection_params={'user': 'root'})
    configure_oar.run()
    logger.info("OAR logger configured on master")

    # propagate SSH keys
    logger.info("configuring OAR SSH")
    oar_ssh_dir = tmp_dir + "/.ssh"

    # Get key from master
    Process('scp -o BatchMode=yes -o PasswordAuthentication=no '
            '-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null '
            '-o ConnectTimeout=20 -rp -o User=root ' +
            master.address + ":/var/lib/oar/.ssh"
            ' ' + oar_ssh_dir).run()
    # Push the key to the slaves
    Put(slaves, [oar_ssh_dir], "/var/lib/oar/",
        connection_params={'user': 'root'}).run()

    # Add resources to OAR
    logger.info("register resources")
    add_resources_cmd = """
    oarproperty -a cpu || true; \
    oarproperty -a core || true; \
    oarproperty -c -a host || true; \
    oarproperty -a mem || true; \
    """
    # List resources
    for node in slaves:
        node_info = [n for n in nodes_info if n["address"] == node.address][0]
        add_resources_cmd = (
            add_resources_cmd +
            "oarnodesetting -a -h {address} -p host={address} -p cpu={nb_proc} "
            "-p core={nb_proc} "
            "-p cpuset=0 -p mem={memory}; \\\n"
        ).format(**node_info)
    # Do add resources
    add_resources = SshProcess(add_resources_cmd, master,
                               connection_params={'user': 'root'})
    add_resources.run()

    if add_resources.ok:
        logger.info("oar is now configured!")
    else:
        logger.error("oar is NOT configured:\n{}".format(
            Report([add_resources])))
        raise RuntimeError("error in the OAR configuration: Abort!")


def activate_OAR_Prologue_Epilogue(master, slaves):

    # Change log directory and enable epilogue prologue
    configure_oar_cmd = """
    sed -i \
      -e 's#^\#\(SERVER_PROLOGUE_EPILOGUE_TIMEOUT\)\=.*#\\1="30"#' \
      -e 's#^\#\(SERVER_PROLOGUE_EXEC_FILE\)\=.*#\\1="/etc/oar/server_prologue"#' \
      -e 's#^\#\(SERVER_EPILOGUE_EXEC_FILE\)\=.*#\\1="/etc/oar/server_epilogue"#' \
      /etc/oar/oar.conf
    """
    configure_oar = Remote(configure_oar_cmd, [master],
                           connection_params={'user': 'root'})
    configure_oar.run()
    logger.info("OAR serveur pro/epilogue is activated")

    # import and set prologue and epilogue
    epilogue = script_path + "/config/server_epilogue"
    prologue = script_path + "/config/server_prologue"
    Put([master], [prologue, epilogue], "/etc/oar/",
        connection_params={'user': 'root'}).run()

    # import Hadoop auxiliary script for pro/epilogue
    aux_scripts = [script_path + "/config/hadoop_nm_start.sh",
                   script_path + "/config/hadoop_nm_stop.sh",
                   script_path + "/config/wait_for_nm.sh"]
    Put([master], aux_scripts, "/opt/hadoop/etc/hadoop/",
        connection_params={'user': 'root'}).run()

    # set the permission on prologue/epilogue files
    SshProcess("chmod 755 /etc/oar/server_*", master,
               connection_params={'user': 'root'}).run()

    # install pyyaml to make the epilogue/prologue works
    SshProcess("apt install python-yaml", master,
               connection_params={'user': 'root'}).run()

    # add OAR to spark group
    Remote("adduser oar spark", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add OAR to hadoop group
    Remote("adduser oar hadoop", [master] + slaves,
           connection_params={'user': 'root'}).run()

    # add authorization for OAR to act as hadoop using passwordless su
    auth_cmd = """\
sed -i '/auth       sufficient pam_rootok.so/a\
auth       [success=ignore default=1] pam_succeed_if.so user = hadoop\\n\
auth       sufficient   pam_succeed_if.so use_uid user = oar' \
/etc/pam.d/su"""

    Remote(auth_cmd, [master] + slaves,
           connection_params={'user': 'root'}).run()


def configure_SSH(tmp_dir, master, slaves):
    # Configure SSH for Hadoop install
    logger.info("enable master SSH connection to slaves and himself")
    ssh_key = tmp_dir + "/id_rsa"
    Process('rm -rf ' + ssh_key).run()
    Process("ssh-keygen -P '' -f {}".format(ssh_key)).run()
    Put([master] + slaves, [ssh_key + ".pub"], "/root/",
        connection_params={'user': 'root'}).run()
    public_key = Remote("cat /root/id_rsa.pub >> ~/.ssh/authorized_keys",
                        [master] + slaves,
                        connection_params={'user': 'root'})
    public_key.run()

    ssh_config = script_path + '/config/config'
    private_key = Put([master], [ssh_config, ssh_key],
                      "/root/.ssh/",
                      connection_params={'user': 'root'})
    private_key.run()
    if public_key.ok and private_key.ok:
        logger.info("SSH is now configured!")
    else:
        logger.error("SSH is NOT configured:\n{}".format(
            Report([public_key, private_key])))
        raise RuntimeError("error in the OAR configuration: Abort!")


def configure_Hadoop(tmp_dir, master, slaves):
    # Configure slaves
    logger.info("configure Hadoop slaves")

    # Configure DataNodes

    # Add DataNodes list in $HADOOP_HOME/etc/hadoop/slaves
    slaves_list = tmp_dir + "/slaves"
    with open(slaves_list, "w") as slave_file:
        for slave in slaves:
            slave_file.write("{}\n".format(slave.address))

    logger.info("configure HDFS")
    # Generate core site config
    site_config_header = """<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>"""

    core_site_config = site_config_header + """
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://{}:9000</value>
    </property>
</configuration>
""".format(master.address)

    core_site_config_file = tmp_dir + '/core-site.xml'
    with open(core_site_config_file, 'w') as text_file:
        text_file.write(core_site_config)

    logger.info("configure YARN")
    # Generate core site config
    yarn_config_header = """<?xml version="1.0" encoding="UTF-8"?>"""

    yarn_site_config = yarn_config_header + """
<configuration>
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>{}</value>
    </property>
    <property>
        <name>yarn.nodemanager.health-checker.interval-ms</name>
        <value>30000</value>
    </property>
    <property>
        <name>yarn.resourcemanager.nodes.exclude-path</name>
        <value>/opt/hadoop/etc/hadoop/yarn.exclude</value>
        <final>true</final>
    </property>
</configuration>
""".format(master.address)

    yarn_site_config_file = tmp_dir + '/yarn-site.xml'
    with open(yarn_site_config_file, 'w') as text_file:
        text_file.write(yarn_site_config)

    # Get HDFS config from ./config folder
    hdfs_site_config = script_path + '/config/hdfs-site.xml'

    Put([master] + slaves,
        [slaves_list,
         hdfs_site_config,
         core_site_config_file,
         yarn_site_config_file],
        "/opt/hadoop/etc/hadoop/",
        connection_params={'user': 'root'}
        ).run()

    # set the permission on yarn exclude files
    SshProcess("chmod 664 /opt/hadoop/etc/hadoop/yarn.exclude", master,
               connection_params={'user': 'root'}).run()


def configure_Spark(tmp_dir, master, slaves):
    logger.info("Configuring Spark")
    # Add slave list in $SPARK_HOME/conf/slaves
    slaves_list = tmp_dir + "/slaves"
    with open(slaves_list, "w") as slave_file:
        for slave in slaves:
            slave_file.write("{}\n".format(slave.address))

    # Configure eventLog directory
    create_HDFS_user_home(master, 'spark')

    events_log_dir = "/user/spark/applicationHistory"
    cmd = ("$HADOOP_HOME/bin/hadoop fs -mkdir -p {events_log_dir};"
           "$HADOOP_HOME/bin/hadoop fs -chown spark:spark {events_log_dir};"
           "$HADOOP_HOME/bin/hadoop fs -chmod 1777 {events_log_dir};"
           ).format(events_log_dir=events_log_dir)

    config_event_log = SshProcess(
        cmd, master,
        connection_params={'user': 'root'}, shell=True)
    config_event_log.run()

    # Set default spark config
    spark_config = """
spark.eventLog.dir               hdfs://{master_addr}:9000/{events_log_dir}
spark.eventLog.enabled           true
spark.history.fs.logDirectory    hdfs://{master_addr}:9000/{events_log_dir}
spark.master                     spark://{master_addr}:7077
spark.task.maxFailures           25
spark.yarn.max.executor.failures 25
""".format(events_log_dir=events_log_dir, master_addr=master.address)

    spark_config_file = tmp_dir + '/spark-defaults.conf'
    with open(spark_config_file, 'w') as text_file:
        text_file.write(spark_config)

    # Push the files to the nodes
    Put([master] + slaves,
        [slaves_list, spark_config_file],
        "/opt/spark/conf/",
        connection_params={'user': 'root'}
        ).run()

    logger.info("Spark is now configured")


def format_HDFS(master, force=False):
    """ format HDFS filesystem (this is requiered to start HDFS)"""
    logger.info("format HDFS")

    format_cmd = "$HADOOP_HOME/bin/hdfs namenode -format -nonInteractive"

    if force:
        logger.warn("format will be FORCED, all data will be erased!")
        format_cmd += " -force"

    format_hdfs = SshProcess(format_cmd,
                             master,
                             connection_params={'user': 'root'},
                             shell=True)
    format_hdfs.run()
    if format_hdfs.ok:
        logger.info("HDFS is now formatted!")
    else:
        logger.error("HDFS is NOT formatted:\n{}".format(
            Report([format_hdfs])))
        raise RuntimeError("error in the HDFS format process: Abort!")


def start_HDFS(master):
    """ start HDFS namenode and datanodes"""
    logger.info("start HDFS daemon")
    start_hdfs = SshProcess("$HADOOP_HOME/sbin/start-dfs.sh", master,
                            connection_params={'user': 'root'}, shell=True)
    start_hdfs.run()
    logger.info('HDFS succesfully started '
                'WebUI:\nhttp://{}:50070'.format(master.address))


def start_YARN(master, slaves):
    """ start YARN from the master (it also starts the slaves if defined"""
    logger.info("start YARN daemons")
    start_yarn = SshProcess(
        'su - hadoop -c "$HADOOP_HOME/sbin/yarn-daemon.sh start resourcemanager"',
        master,
        connection_params={'user': 'root'}, shell=True)
    start_yarn.run()

    Remote('su - hadoop -c "$HADOOP_HOME/sbin/yarn-daemon.sh start nodemanager"',
           slaves,
           connection_params={'user': 'root'}).run()

    logger.info('Hadoop YARN '
                'WebUI:\nhttp://{}:8088'.format(master.address))


def create_HDFS_user_home(master, username):
    """ create a home in HDFS for a user to allows him to write in HDFS """
    home_dir = "/user/" + username
    logger.info("create home directory: " + home_dir)
    cmd = "$HADOOP_HOME/bin/hadoop fs -mkdir -p " + home_dir

    create_home = SshProcess(cmd, master,
                             connection_params={'user': 'root'}, shell=True)
    create_home.run()

    cmd = "$HADOOP_HOME/bin/hadoop fs -chown " + username + " " + home_dir

    set_acl_home = SshProcess(
        cmd, master,
        connection_params={'user': 'root'}, shell=True)
    set_acl_home.run()


def start_Spark_standalone(master, slaves):
    # Start Spark master
    logger.info("start Spark master daemon")
    start_spark_master = SshProcess("$SPARK_HOME/sbin/start-master.sh",
                                    master,
                                    connection_params={'user': 'root'},
                                    shell=True)
    start_spark_master.run()
    logger.info('Spark '
                'WebUI:\nhttp://{}:8080'.format(master.address))

    # Start Spark slaves
    logger.info("start Spark slaves daemon")
    start_spark_slaves = Remote(
        "sudo -u oar $SPARK_HOME/sbin/start-slave.sh spark://{}:7077".format(
            master.address),
        slaves,
        connection_params={'user': 'root'})
    start_spark_slaves.run()

    start_Spark_history_server(master)


def start_Spark_history_server(master):
    # Start Spark history server
    logger.info("start Spark history server daemon")
    start_spark_hist = SshProcess("$SPARK_HOME/sbin/start-history-server.sh",
                                  master,
                                  connection_params={'user': 'root'},
                                  shell=True)
    start_spark_hist.run()

    logger.info('Spark History server '
                'WebUI:\nhttp://{}:18080'.format(master.address))


def generate_data(master, data_size_GB):
    gen_script = script_path + "/config/genData_MicroBenchmarks.sh"
    Put([master], [gen_script], "~/BigDataBench/MicroBenchmarks",
        connection_params={'user': 'root'}).run()
    logger.info("Generating a dataset of {}GB...".format(data_size_GB))
    cmd = """
cd ~/BigDataBench/MicroBenchmarks;
test -d /tmp/data-MicroBenchmarks_{size} || ./genData_MicroBenchmarks.sh /tmp data-MicroBenchmarks_{size} <<EOF
{size}
EOF""".format(size=data_size_GB)

    SshProcess(cmd, master, connection_params={'user': 'root'}).run()
    logger.info("Succesful dataset generation")


def gather_bigdata_jobs_info(master, result_dir):
    logger.info("Gathering Big Data jobs informations")
    # Get YARN app info
    url = "http://{}:8088/ws/v1/cluster/apps".format(master.address)
    with open(result_dir + "/hadoop_apps.json", "w+") as myfile:
        myfile.write(json.dumps(json.load(urllib2.urlopen(url)), indent=4))

    # Get Spark job info
    url = "http://{}:18080/api/v1/applications".format(master.address)
    apps = json.load(urllib2.urlopen(url))
    with open(result_dir + "/spark_apps.json", "w+") as myfile:
        myfile.write(json.dumps(apps, indent=4))
    for app in apps:
        parser = re.compile('drawApplicationTimeline(\(.*?\));', re.DOTALL)
        events = []

        for attempt in app["attempts"]:
            app_id = app["id"] + "/" + attempt["attemptId"]
            url = "http://{}:18080/api/v1/applications/{}/executors".format(
                master.address,
                app_id)
            with open(result_dir + "/{}_{}.json".format(app["id"], attempt["attemptId"]), "w+") as myfile:
                myfile.write(json.dumps(json.load(urllib2.urlopen(url)), indent=4))

            # Get info from Spark UI because it is not available in REST API
            spark_ui_url = "http://{}:18080/history/{}/jobs/".format(master.address, app_id)
            html = urllib2.urlopen(spark_ui_url).read()
            matches = parser.search(html)

            match = matches.group(1)
            clean = re.sub(r'new Date\((.*)\)', r'\1', match)

            elems = eval(clean)[1]
            for elem in elems:
                if elem['group'] != 'executors':
                    continue
                event = {}
                if elem['className'] == 'executor added':
                    event['starting_time'] = elem['start']
                if elem['className'] == 'executor removed':
                    event['finish_time'] = elem['start']

                event['executor_id'] = re.match(".*Executor ([0-9]+)<br>.*",
                                                elem['content']).group(1)
                event['app_id'] = app["id"]
                event['attempt_id'] = attempt["attemptId"]
                events.append(event)
        with open(result_dir + '/{}_events.json'.format(app["id"]), 'a+') as outfile:
            json.dump(events, outfile)


def gather_nodes_information(nodes, result_dir):
    info_gather = Remote("nproc; awk '/MemTotal/ {print $2}' /proc/meminfo",
                         nodes)
    info_gather.run()
    nodes_info = []
    for process in info_gather.processes:
        nodes_info.append({"address": process.host.address,
                           "nb_proc": process.stdout.split('\n')[0].strip(),
                           "memory": process.stdout.split('\n')[1].strip()})

    with open(result_dir + "/nodes_info.json", "w+") as nodes_file:
        json.dump(nodes_info, nodes_file)

    return nodes_info


def prediction_callback(ts):
    logger.info("job start prediction = {}".format(format_date(ts)))


class BeBiDaExperiment(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--oar_job_id',
                          help="Grid'5000 OAR job ID")
        parser.add_option('--force_reconfig',
                          action='store_true',
                          default=False,
                          help='if set and oar_job_id is set, the cluster'
                          'is re-configured')
        parser.add_option('--force_hdfs_format',
                          action='store_true',
                          default=False,
                          help='if set, HDFS format will be forced')
        parser.add_option('--force_redeploy',
                          action='store_true',
                          default=False,
                          help='if set, Node re-deployment will be forced')
        parser.add_option('--test', dest='is_a_test',
                          action='store_true',
                          default=False,
                          help='prefix the result folder with "test", enter '
                          'a debug mode if fails and remove the job '
                          'afterward, unless it is a reservation')

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def setup_result_dir(self):
        is_a_test = self.options.is_a_test

        if is_a_test:
            result_dir = "test_results"
        else:
            result_dir = "results"
        try:
            os.mkdir(result_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise exc
        self.result_dir = (script_path + '/' + result_dir + '/results_' +
                           time.strftime("%Y-%m-%d--%H-%M-%S"))

    def run(self):
        """Run the experiment"""
        logger.debug("Start BeBiDaExperiment.run() method")

        # get options
        force_reconfig = self.options.force_reconfig
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None
        is_a_test = self.options.is_a_test
        force_hdfs_format = self.options.force_hdfs_format
        force_redeploy = self.options.force_redeploy

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # import configuration
        with open(self.args[0]) as config_file:
            config = json.load(config_file)

        # backup configuration
        copy(self.args[0], self.result_dir)

        # load configuration
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])
        slave_env = config["kadeploy_slave_env"]
        master_env = config["kadeploy_master_env"]
        logger.info("You ask for {} resources during {} "
                    "time".format(resources, walltime))
        experiments = config.get("experiments", [])

        try:
            # get ressources
            job_id, nodes = get_resources(
                resources, walltime, site, oar_job_id)

            # master is the first node of the reservation others are slaves
            master = nodes[0]
            slaves = nodes[1:]

            # do deployment
            deploy_images(master, master_env, slaves, slave_env,
                          force_redeploy=force_redeploy)

            # store node list
            nodes_info = gather_nodes_information(nodes, self.result_dir)

            # Configuration
            if (not oar_job_id) or force_reconfig:
                # Create temporary directory
                tmp_dir = self.result_dir + '/tmp'
                Process('mkdir ' + tmp_dir).run()

                configure_OAR(tmp_dir, self.result_dir, master, slaves,
                              nodes_info)

                # needed for HDFS and Spark setup
                configure_SSH(tmp_dir, master, slaves)

                configure_Hadoop(tmp_dir, master, slaves)

                try:
                    format_HDFS(master, force=force_hdfs_format)
                except:
                    pass

                start_HDFS(master)

                configure_Spark(tmp_dir, master, slaves)

                create_HDFS_user_home(master, os.getlogin())

                start_YARN(master, slaves)

                start_Spark_history_server(master)

                #start_Spark_standalone(master, slaves)

            activate_OAR_Prologue_Epilogue(master, slaves)

            # FIXME: put this in the kameleon recipe
            # install time for experiments
            SshProcess("apt install time", master,
                       connection_params={'user': 'root'}).run()

            # define the iterator over the parameters combinations
            self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                        sweep(experiments))

            # Due to previous (using -c result_dir) run skip some combination
            logger.info('Skipped parameters:' +
                        '{}'.format(str(self.sweeper.get_skipped())))

            logger.info('combinations {}'.format(
                str(self.sweeper.get_remaining())))

            while len(self.sweeper.get_remaining()) > 0:
                logger.info('Remaining combinations {}'.format(
                    str(len(self.sweeper.get_remaining()))))
                expe = self.sweeper.get_next()
                logger.info("Execute experiment: {}".format(expe))

                nb_nodes = expe.get("nb_nodes", 1)
                data_size_GB = expe.get("data_size_GB", 1)
                running_jobs = expe.get("running_jobs", 0)
                # Use one executor per nodes
                nb_executors = len(slaves)

                generate_data(master, data_size_GB)

                # First run a job HPC and BD to load the cluster
                result_dir = self.result_dir

                expe_script_path = script_path + "/rjms_overhead"

                cmd = "{}/bebida_rjms_job_start_overhead.sh {} {}".format(
                        expe_script_path, nb_nodes, result_dir)

                prep_expe = SshProcess(cmd,
                                       master,
                                       connection_params={
                                           "user": os.getlogin()})
                prep_expe.stdout_handlers.append(self.result_dir + '/stage_prep.out')
                logger.info("Run preparation expe")
                prep_expe.run()
                # Submit a Spark job to have the righe numbers of executors running
                spark_submit_cmd = """/opt/spark/bin/spark-submit \
        --master yarn \
        --deploy-mode cluster \
        --class cn.ac.ict.bigdatabench.Sort \
        --driver-memory 2G \
        --executor-memory 2G \
        --executor-cores 4 \
        --num-executors {nb_executors} \
        /opt/BigDataBench_V3.2.5_Spark/JAR_FILE/bigdatabench-spark_1.3.0-hadoop_1.0.4.jar \
        /data-MicroBenchmarks_{data_size} \
        spark-sort-result{nb_job}""".format(
                    nb_executors=nb_executors,
                    nb_job=nb_nodes,
                    data_size=data_size_GB
                )

                # Start spark jobs jobs
                logger.info("Start Spark jobs...")
                spark_jobs = []
                for i in xrange(running_jobs):
                    job = SshProcess(spark_submit_cmd, master)
                    job.stdout_handlers.append(self.result_dir +
                                               '/spark_job_{}.out'.format(i))
                    job.stderr_handlers.append(self.result_dir +
                                               '/spark_job_{}.err'.format(i))
                    spark_jobs.append(job.start())
                logger.info("Spark jobs started")

                # Then, do the prologue epilogue manually to see the real
                # overhead
                cmd = 'su - oar -c ' +\
                    '"{}/bebida_rjms_job_start_overhead_oar.sh {} {} {}"'.format(
                        expe_script_path, nb_nodes, result_dir,
                        running_jobs)
                expe_2stage = SshProcess(cmd,
                                         master,
                                         connection_params={"user": "root"})
                expe_2stage.stdout_handlers.append(self.result_dir + '/stage_2.out')
                logger.info("Run actual experiment")
                expe_2stage.run()

                # Stop jobs
                logger.info("Kill remaining Spark jobs")
                for spark_job in spark_jobs:
                    spark_job.kill()

                if expe_2stage.ok:
                    logger.info("Expe OK: {}".format(expe))
                    self.sweeper.done(expe)
                else:
                    logger.info("Expe NOT OK: {}".format(expe))
                    self.sweeper.cancel(expe)
                    raise RuntimeError("error during the experiment: Abort!")

            # Gather application information
            gather_bigdata_jobs_info(master, result_dir)

        except:
            traceback.print_exc()
            ipdb.set_trace()
        finally:
            if is_a_test:
                ipdb.set_trace()
            if oar_job_id is None:
                logger.info("delete job: {}".format(job_id))
                oardel([(job_id, site)])


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, lambda *args: ipdb.set_trace())
    engine = BeBiDaExperiment()
    engine.start()
