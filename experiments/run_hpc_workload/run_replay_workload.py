#!/usr/bin/env python2
"""
This script is running a complete OAR cluster deployment on Grid'5000 to
experiment OAR scheduling policies in real conditions.

It use the development version of an OAR scheduler name Kamelot

It give as an results information about scheduling that was done.
"""
import os
import sys
from shutil import copy
from execo import Process, SshProcess
from execo_g5k import oardel
from execo_engine import Engine, logger, ParamSweeper, sweep
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import expe_tools
from expe_tools import \
    get_resources, deploy_images, configure_OAR, \
    load_experiment_config, gather_nodes_information

script_path = os.path.dirname(os.path.realpath(__file__))

is_a_test = False

reservation_job_id = None


class ReplayHPCworkload(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--is_a_test',
                          dest='is_a_test',
                          action='store_true',
                          default=False,
                          help='prefix the result folder with "test", enter '
                          'a debug mode if fails and remove the job '
                          'afterward, unless it is a reservation')
        parser.add_option('--force_reconfig',
                          action='store_true',
                          default=False,
                          help='if set and oar_job_id is set, the cluster'
                          'is re-configured')
        parser.add_option('--force_redeploy',
                          help='can be all, master, slaves')
        parser.add_option('--oar_job_id',
                          help="Grid'5000 reservation job ID")
        parser.add_option('--oar_inner_job_id',
                          help="Grid'5000 container to schedule in job ID")


        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir(is_a_test)

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None
        oar_inner_job_id = int(self.options.oar_inner_job_id) \
            if self.options.oar_inner_job_id is not None else None
        is_a_test = self.options.is_a_test
        force_reconfig = self.options.force_reconfig

        if is_a_test:
            logger.warn('THIS IS A TEST! This run will use only a few '
                        'resources')

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # get configuration
        config = load_experiment_config(self.args[0], self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])

        slave_env = config["kadeploy_slave_env"]
        master_env = config["kadeploy_master_env"]
        workloads = config["workloads"]
        required_resource_nb = config["required_resource_nb"]
        # check if workloads exists (Suppose that the same NFS mount point
        # is present on the remote and the local environment
        for workload_file in workloads:
            with open(workload_file):
                pass
            # copy the workloads files to the results dir
            copy(workload_file, self.result_dir)

        # define the workloads parameters
        self.parameters = {
            'workload_filename': workloads
        }
        logger.info('Workloads: {}'.format(workloads))

        # define the iterator over the parameters combinations
        self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                    sweep(self.parameters))

        # Due to previous (using -c result_dir) run skip some combination
        logger.info('Skipped parameters:' +
                    '{}'.format(str(self.sweeper.get_skipped())))

        logger.info('Number of parameters combinations {}'.format(
            str(len(self.sweeper.get_remaining()))))
        logger.info('combinations {}'.format(
            str(self.sweeper.get_remaining())))

        options = "--name={}".format(self.run_name)
        if oar_inner_job_id:
            options = "{} -t inner={}".format(options, oar_inner_job_id)
        job_id, nodes = get_resources(resources, walltime, site,
                                      oar_job_id=oar_job_id,
                                      options=options)

        # master is the first node of the reservation others are slaves
        master = nodes[0]
        slaves = nodes[1:required_resource_nb]

        # update the nodes with only the selected ones
        nodes = [master] + slaves

        if len(slaves) + 1 != required_resource_nb:
            raise RuntimeError("Number of available nodes {} is not equals to"
                               " the number of required resources {}".format(
                                   len(slaves) + 1, required_resource_nb))

        # do deployment
        deploy_images(master, master_env, slaves, slave_env,
                      force_redeploy=self.options.force_redeploy,
                      required_resource_nb=required_resource_nb)


        # store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)

        # Configuration
        # Create temporary directory
        tmp_dir = self.result_dir + '/tmp'
        Process('mkdir -p ' + tmp_dir).run()

        # Configure RJMS
        configure_OAR(tmp_dir, self.result_dir, master, slaves,
                      nodes_info)

        # Do the replay
        logger.info('Starting to replay')
        while len(self.sweeper.get_remaining()) > 0:
            combi = self.sweeper.get_next()
            workload_file = os.path.basename(combi['workload_filename'])
            oar_replay = SshProcess(script_path + "/oar_replay.py " +
                                    combi['workload_filename'] + " " +
                                    self.result_dir + "  oar_gant_" +
                                    workload_file,
                                    master)
            oar_replay.stdout_handlers.append(self.result_dir + '/' +
                                              workload_file + '.out')
            logger.info("replaying workload: {}".format(combi))
            oar_replay.run()
            if oar_replay.ok:
                logger.info("Replay workload OK: {}".format(combi))
                self.sweeper.done(combi)
            else:
                logger.info("Replay workload NOT OK: {}".format(combi))
                self.sweeper.cancel(combi)
                raise RuntimeError("error in the OAR replay: Abort!")

        if oar_inner_job_id is not None:
            oardel([(job_id, site)])

if __name__ == "__main__":
    engine = ReplayHPCworkload()
    expe_tools.script_path = script_path
    engine.start()
