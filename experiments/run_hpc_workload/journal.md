# Experiment journal

The HPC workload is made with this script:

```sh
nb_job=150
for seed in $(seq 1 20)
do
  ./json_workload_generator.py \
    delay_profiles_mpi_commands.json \
    workloads/workload_seed${seed}_jobs${nb_job}.json 32 \
    --indent 4 \
    --random_seed $seed \
    --job_number $nb_job \
    --job_log_size_sigma 3 \
    --job_iarrival_lambda 40 \
    --job_iarrival_k 1 \
    --maximum_power_of_two 5
done
```

Then we run batsim with a simple scheduler on several to choose two
different version for a low and high utilisation. First get pybatsim:
```sh
cd ~/Project
git clone https://gitlab.inria.fr/batsim/pybatsim.git
```
To run batsim in the current folder:
```sh
for seed in $(seq 1 20)
do
docker run --net host -u $(id -u):$(id -g) -v $PWD:/data oarteam/batsim:2017-04-14 \ 
  -p ./energy_platform_homogeneous_no_net_32.xml \
  -w ./workloads/workload_seed${seed}_jobs150.json \
  -e results/simulated/seed${seed}_jobs150 &

python ~/Projects/pybatsim/launcher.py easyBackfillNotopo

done
```

Then check workloads and the seed3 with 76% of utilisation is selected for
high load, and seed5 with 47% utilisation for low utilisation. Yau can get
more details here:

```python
from evalys.jobset import JobSet
import  matplotlib.pyplot as plt
plt.ion()
j3 = JobSet.from_csv("results/simulated/seed3_jobs150_jobs.csv")
j3.plot(with_details=True, normalize=True)
j5 = JobSet.from_csv("results/simulated/seed5_jobs150_jobs.csv")
j5.plot(with_details=True, normalize=True)
```

Then run the workload replay on grid5000 with:
```sh
ipython --pdb -- run_replay_workload.py expe_graphene_seed3and5.json
```

The results can be converted to batsim output format in order to visualize
it in evalys:

```sh
/oar_result_to_batsim_csv.py \
  -m results/results_2017-04-14--18-45-53/workload_seed3_jobs150-job_id_mapping.csv \
  results/results_2017-04-14--18-45-53/oar_gant_workload_seed3_jobs150.json \
  results/real/results_seed3_jobs150.csv

```

And visualyze like the previous simulated resutls:
```python
import evalys.jobset import JobSet
import  matplotlib.pyplot as plt
plt.ion()
j3 = JobSet.from_csv("results_seed3_jobs150.csv")
j3.plot(with_details=True, normalize=True)
j5 = JobSet.from_csv("results_seed5_jobs150.csv")
j5.plot(with_details=True, normalize=True)
```
