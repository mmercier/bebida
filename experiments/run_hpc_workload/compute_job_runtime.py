#!/usr/bin/env python
"""
This script is made to run each benchmarks of the NPP isolated under a switch
to get optimal runtime for each benchmarks
"""
import os
import sys
import traceback
from execo import Process, SshProcess, format_date
from execo_g5k import oarsub, OarSubmission, \
    get_oar_job_nodes, wait_oar_job_start, \
    get_cluster_site
from execo_engine import Engine, ParamSweeper, sweep, slugify, logger

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))
import expe_tools
from expe_tools import get_resources, load_experiment_config, gather_nodes_information

script_path = os.path.dirname(os.path.realpath(__file__))


def prediction_callback(ts):
    logger.info("job start prediction = %s" % (format_date(ts),))


class ComputeJobRuntime(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--oar_job_id',
                          help="Grid'5000 reservation job ID")

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')


    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None

        config = load_experiment_config(self.args[0], self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])

        # define parameter consistent with the kadeploy image
        mpi_options = ('--mca btl_tcp_if_exclude ib0,ib1,lo,eth2,eth1 '
                       '--mca btl self,sm,tcp')

        npb_bin_path = script_path + '/bin'

        parameters = config["parameters"]
        nb_procs = parameters["nb_procs"]
        size = parameters["size"]

        # define the iterator over the parameters combinations
        self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                    sweep(parameters))

        # Due to previous (using -c result_dir) run skip some combination
        logger.info('Skipped parameters:'
                    '{}'.format(str(self.sweeper.get_skipped())))

        logger.info('Number of parameters combinations {}'.format(
            str(len(self.sweeper.get_remaining()))))

        # go to the result folder before everything
        os.chdir(self.result_dir)

        job_id, nodes = get_resources(resources, walltime, site,
                                      oar_job_id=oar_job_id,
                                      options="-t allow_classic_ssh",
                                      job_type=None)

        # store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)
        # we considere the cluster homogeneous
        procs_per_node = int(nodes_info[0]["nb_proc"])

        logger.info("create hostfiles for all combinations")
        for procs in nb_procs:
            hostfile_filename = '{}/hostfile-{}'.format(
                self.result_dir,
                procs)
            with open(hostfile_filename, 'w') as hostfile:
                for node in nodes[:(int(procs) / int(procs_per_node))]:
                    for proc_num in range(procs_per_node):
                        print>>hostfile, node.address

        result_file = "{}/execution_time.csv".format(self.result_dir)
        # Iterate over the parameters and execute the bench
        while len(self.sweeper.get_remaining()) > 0:
            try:
                comb = self.sweeper.get_next()
                logger.info('Processing new combination %s' % (comb,))
                #if (comb['bench'] == 'lu' and comb['size'] == 'D'):
                #    logger.info('skip this combination')
                #    self.sweeper.skip(comb)
                #    continue

                mpi_command = '{}.{}.{}'.format(comb['bench'],
                                                comb['size'],
                                                comb['nb_procs'])

                hostfile_filename = self.result_dir + \
                    '/' + 'hostfile-' + comb['nb_procs']
                bench_cmd = (
                    '/usr/bin/time -f "{mpi_command} %e" -a '
                    '-o {result_file} '
                    'mpirun {option} -hostfile {hostfile} {full_command} > '
                    '{result_dir}/{mpi_command}'
                ).format(
                    option=mpi_options,
                    hostfile=hostfile_filename,
                    full_command=npb_bin_path + "/" + mpi_command,
                    mpi_command=mpi_command,
                    result_file=result_file,
                    result_dir=self.result_dir)

                run_job = SshProcess("cd " + self.result_dir +
                                     "; " + bench_cmd, nodes[0])
                run_job.stdout_handlers.append(
                    self.result_dir + '/' + slugify(comb) + '.out')
                logger.info("generate command: {}".format(bench_cmd))
                run_job.run()
                if run_job.ok:
                    logger.info("comb ok: %s" % (comb,))
                    self.sweeper.done(comb)
                else:
                    #Process("sed -i '$ d' {}".format(result_file)).run()
                    pass

            except Exception as exception:
                traceback.print_exc()
                logger.warn("comb NOT ok: %s" % (comb,))
                self.sweeper.cancel(comb)


if __name__ == "__main__":
    engine = ComputeJobRuntime()
    engine.start()
