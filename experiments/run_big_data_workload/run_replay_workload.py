#!/usr/bin/env python3
"""
This script is running a complete OAR cluster deployment on Grid'5000 to
experiment OAR scheduling policies in real conditions.

It use the development version of an OAR scheduler name Kamelot

It give as an results information about scheduling that was done.
"""
import os
import sys
import json
from shutil import copy
from execo import Process, SshProcess, Remote
from execo_engine import Engine, logger, ParamSweeper, sweep
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import expe_tools
from expe_tools import (
    get_resources, deploy_images, configure_SSH, load_experiment_config,
    gather_nodes_information, configure_Hadoop, format_HDFS, start_HDFS,
    stop_HDFS, configure_Spark, create_HDFS_user_home, stop_YARN, start_YARN,
    stop_Spark_history_server, start_Spark_history_server, generate_data,
    push_dataset_to_HDFS, mount_storage5k
)


script_path = os.path.dirname(os.path.realpath(__file__))


class ReplayBigDataWorkload(Engine):

    def init(self):
        parser = self.options_parser
        parser.add_option('--is_a_test',
                          dest='is_a_test',
                          action='store_true',
                          default=False,
                          help='prefix the result folder with "test", enter '
                          'a debug mode if fails and remove the job '
                          'afterward, unless it is a reservation')
        parser.add_option('--skip_reconfig',
                          action='store_true',
                          default=False,
                          help='if set and oar_job_id is set, the cluster'
                          'is re-configured')
        parser.add_option('--skip_data_ingestion',
                          action='store_true',
                          default=False,
                          help='if set skip dataset ingestion to HDFS')
        parser.add_option('--force_redeploy',
                          help='can be all, master, slaves')
        parser.add_option('--force_hdfs_format',
                          action='store_true',
                          default=False,
                          help='if set, HDFS format will be forced')
        parser.add_option('--oar_job_id',
                          help="Grid'5000 reservation job ID")

        parser.add_argument('experiment_config',
                            'The config JSON experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir(self.options.is_a_test)

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.options.oar_job_id) \
            if self.options.oar_job_id is not None else None
        is_a_test = self.options.is_a_test
        skip_reconfig = self.options.skip_reconfig
        skip_data_ingestion = self.options.skip_data_ingestion
        force_hdfs_format = self.options.force_hdfs_format

        if is_a_test:
            logger.warn('THIS IS A TEST! This run will use only a few '
                        'resources')

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # get configuration
        config = load_experiment_config(self.args[0], self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        resources = config["resources"]
        walltime = str(config["walltime"])

        slave_env = config["kadeploy_slave_env"]
        master_env = config["kadeploy_master_env"]
        workloads = config["workloads"]
        required_resource_nb = config["required_resource_nb"]
        data_dir = config.get("data_dir", None)
        logger.info(config)
        # check if workloads exists (Suppose that the same NFS mount point
        # is present on the remote and the local environment
        for workload_file in workloads:
            with open(workload_file):
                pass
            # copy the workloads files to the results dir
            copy(workload_file, self.result_dir)

        # define the workloads parameters
        self.parameters = {
            'workload_filename': workloads
        }
        logger.info('Workloads: {}'.format(workloads))

        # define the iterator over the parameters combinations
        self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
                                    sweep(self.parameters))

        # Due to previous (using -c result_dir) run skip some combination
        logger.info('Skipped parameters:' +
                    '{}'.format(str(self.sweeper.get_skipped())))

        logger.info('Number of parameters combinations {}'.format(
            str(len(self.sweeper.get_remaining()))))
        logger.info('combinations {}'.format(
            str(self.sweeper.get_remaining())))

        job_id, nodes = get_resources(resources, walltime, site,
                                      oar_job_id=oar_job_id)

        # master is the first node of the reservation others are slaves
        master = nodes[0]
        slaves = nodes[1:required_resource_nb]

        # update the nodes with only the selected ones
        nodes = [master] + slaves

        if len(slaves) + 1 != required_resource_nb:
            raise RuntimeError("Number of available nodes {} is not equals to"
                               " the number of required resources {}".format(
                                   len(slaves) + 1, required_resource_nb))

        # do deployment
        deploy_images(master, master_env, slaves, slave_env,
                      force_redeploy=self.options.force_redeploy,
                      required_resource_nb=required_resource_nb)

        # hdfs home dir
        home_dir = "/user/" + os.getlogin()

        if not skip_reconfig:
            # Configuration
            # Create temporary directory
            tmp_dir = self.result_dir + '/tmp'
            Process('mkdir -p ' + tmp_dir).run()

            # needed for HDFS and Spark setup
            configure_SSH(tmp_dir, master, slaves)

            # store node list
            nodes_info = gather_nodes_information(nodes, self.result_dir)

            configure_Hadoop(tmp_dir, master, slaves, nodes_info)

            try:
                stop_HDFS(master)
                format_HDFS(master, slaves, force=force_hdfs_format)
            except Exception as e:
                logger.info(e)

            #stop_HDFS(master)
            start_HDFS(master)

            configure_Spark(tmp_dir, master, slaves, nodes_info)

            create_HDFS_user_home(master, os.getlogin())

            stop_YARN(master, slaves)
            start_YARN(master, slaves)

            stop_Spark_history_server(master)
            start_Spark_history_server(master)

        if not skip_data_ingestion:
            # Mount the data directory
            mount_storage5k(nodes, data_dir)

            # get all profiles
            profiles = {}
            for workload_file in workloads:
                with open(workload_file) as wl:
                    profiles = dict(profiles, **json.load(wl)["profiles"])

            # populate HDFS in parallel (one node per dataset)
            processes = []
            for profile, node in zip(profiles.values(), nodes[:len(profiles)]):
                process = push_dataset_to_HDFS(node, profile, data_dir, home_dir)
                if process is not None:
                    processes.append(process)
            for process in processes:
                process.wait()
                if not process.ok:
                    raise Exception("Error while populating HDFS: {}".format(process.stdout))

        # Do the replay
        logger.info('Starting to replay')
        while len(self.sweeper.get_remaining()) > 0:
            combi = self.sweeper.get_next()
            workload_file = os.path.basename(combi['workload_filename'])
            spark_replay = SshProcess(script_path + "/spark_replay.py " +
                                    combi['workload_filename'] + " " +
                                    self.result_dir + "  spark_gant_" +
                                    workload_file,
                                    master)
            spark_replay.stdout_handlers.append(self.result_dir + '/' +
                                              workload_file + '.out')
            logger.info("replaying workload: {}".format(combi))
            spark_replay.run()
            if spark_replay.ok:
                logger.info("Replay workload OK: {}".format(combi))
                self.sweeper.done(combi)
            else:
                logger.info("Replay workload NOT OK: {}".format(combi))
                self.sweeper.cancel(combi)
                raise RuntimeError("error in the Spark replay: Abort!")


if __name__ == "__main__":
    engine = ReplayBigDataWorkload()
    expe_tools.script_path = script_path
    engine.start()
