#!/usr/bin/env python3
"""
From results of big data workload replay conpute a JobSet and show it
only support python 3.5 or greater
"""
import glob
import os
import sys
import shutil
import pathlib
import aiofiles
import asyncio
import json
import pandas as pd
from evalys.jobset import JobSet
from procset import ProcInt, ProcSet
import logging
# import matplotlib.pyplot as plt
# from evalys import visu

logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)7s: %(message)s',
    stream=sys.stderr,
)
logger = logging.getLogger(__name__)

script_path = os.path.dirname(os.path.realpath(__file__))


def extract_application_zip_logs(path):
    # Extract all applications
    logger.info("To extract path: {}".format(path))
    for to_unzip in glob.glob(path + '/*.zip'):
        zip_file = os.path.basename(to_unzip)
        logger.info("Zip File path: {}".format(zip_file))
        extracted_file = zip_file.split('.')[0]
        if not pathlib.Path(path + extracted_file).exists():
            logger.info("path: {}".format(path))
            shutil.unpack_archive(to_unzip, path)


async def get_events(file_name, nodes, filter_after):
    """ returns a list of events with the given name in the given file """
    logger.info("Get events from {}".format(file_name))
    logger.debug("Given list of nodes {}".format(nodes))
    executors = {}
    nb_tasks_todo = 0
    nb_started_tasks = 0
    nb_complete_tasks = 0
    nb_failed_tasks = 0
    nb_started_stages = 0
    nb_complete_stages = 0
    nb_resubmitted_tasks = 0
    time_in_resubmitted_tasks = 0
    time_in_resubmitted_tasks_after_filter = 0
    time_in_failed_task = 0
    time_in_failed_task_after_filter = 0
    time_in_complete_task = 0
    time_in_complete_task_after_filter = 0
    longest_task_execution_time = 0
    locality = {"NODE_LOCAL": 0,
                "RACK_LOCAL": 0,
                "PROCESS_LOCAL": 0,
                "NO_PREF": 0,
                "ANY": 0}
    async with aiofiles.open(file_name, mode='r') as f:
        async for line in f:
            event = json.loads(line)

            if "SparkListenerEnvironmentUpdate" == event["Event"]:
                requested_cores = \
                    int(event["Spark Properties"]["spark.executor.cores"])

            elif "Application" in event["Event"]:
                logger.debug("Found application event: {}".format(event))
                if event["Event"] == "SparkListenerApplicationStart":
                    app_id = event["App ID"]
                    app_start_timestamp = event['Timestamp']
                    app_type = event["App Name"]

                elif event["Event"] == "SparkListenerApplicationEnd":
                    app_stop_timestamp = event['Timestamp']
                    for executor in executors.values():
                        if "finish_time" not in executor:
                            executor["finish_time"] = app_stop_timestamp

            elif "Executor" in event["Event"]:
                logger.debug("Found executor event: {}".format(event))

                if event["Event"] == "SparkListenerExecutorAdded":
                    executor = {
                        # this is the job_id needed for JobSet
                        "job_id": app_id + "_" + event["Executor ID"],
                        "executor_id": event["Executor ID"],
                        "app_id": app_id,
                        "app_type": app_type,
                        "nb_cores": event["Executor Info"]["Total Cores"],
                        "requested_number_of_processors": requested_cores,
                        "host": event["Executor Info"]["Host"],
                        "starting_time": event['Timestamp'],
                        "submission_time": app_start_timestamp,
                        "failed_tasks": 0,
                        "failed_tasks_time": 0,
                        "success_tasks": 0,
                        "success_tasks_time": 0,
                        "resubmitted_tasks": 0
                    }
                    # Convert host name into resource interval
                    try:
                        node_index = [
                            ((i - 1) * int(item["nb_proc"]) + 1)
                            for i, item in enumerate(reversed(nodes))
                            if item["address"] == executor["host"]
                        ][0]
                    except Exception as e:
                        print(nodes)
                        print(executor)
                        raise
                    logger.debug("Node index={}; executor host={}".format(
                        node_index, executor["host"]))
                    # correct index to fit with OAR 0 based index
                    node_index = node_index - (executor["nb_cores"] * 4)
                    res_interval = ProcInt(
                        node_index, node_index + executor["nb_cores"] - 1)

                    executor["allocated_processors"] = ProcSet(res_interval)

                    executors[event["Executor ID"]] = executor

                elif event["Event"] == "SparkListenerExecutorRemoved":
                    # WARN Be sure that this is the right event name...
                    executor_stopped = event["Timestamp"]
                    executors[
                        event["Executor ID"]]["finish_time"] = executor_stopped

            elif "Task" in event["Event"]:
                logger.debug("Found task event: {}".format(event))

                if event["Event"] == "SparkListenerTaskStart":
                    nb_started_tasks = nb_started_tasks + 1

                elif event["Event"] == "SparkListenerTaskEnd":
                    task_info = event["Task Info"]

                    # filter information on task
                    after_filter = False
                    if filter_after and task_info["Launch Time"] > filter_after * 1000:
                        after_filter = True

                    # get task locality
                    locality[task_info["Locality"]] = \
                            locality[task_info["Locality"]] + 1

                    # get task time
                    task_runtime = task_info[
                        "Finish Time"] - task_info["Launch Time"]

                    # compute longest task
                    longest_task_execution_time = max(
                        longest_task_execution_time, task_runtime)

                    executor = executors[task_info["Executor ID"]]
                    if event["Task End Reason"]["Reason"] == "Resubmitted":
                        # This event is redondant with the end of the task: It
                        # only indicate that the task is resubmitted because
                        # the result was lost.
                        executor["resubmitted_tasks"] = \
                            executor["resubmitted_tasks"] + 1
                        nb_resubmitted_tasks = nb_resubmitted_tasks + 1
                        time_in_resubmitted_tasks = \
                            time_in_resubmitted_tasks + task_runtime
                        if after_filter:
                            time_in_resubmitted_tasks_after_filter = \
                                time_in_resubmitted_tasks_after_filter \
                                + task_runtime
                        continue

                    # append to tasks metrics
                    if task_info["Failed"]:
                        executor["failed_tasks"] = \
                            executor["failed_tasks"] + 1
                        executor["failed_tasks_time"] = \
                            executor["failed_tasks_time"] + task_runtime
                        nb_failed_tasks = nb_failed_tasks + 1
                        time_in_failed_task = time_in_failed_task + task_runtime
                        if after_filter:
                            time_in_failed_task_after_filter = \
                                time_in_failed_task_after_filter \
                                + task_runtime
                    else:
                        executor["success_tasks"] = \
                            executor["success_tasks"] + 1
                        executor["success_tasks_time"] = \
                            executor["success_tasks_time"] + task_runtime
                        nb_complete_tasks = nb_complete_tasks + 1
                        time_in_complete_task = time_in_complete_task + task_runtime
                        if after_filter:
                            time_in_complete_task_after_filter = \
                                time_in_complete_task_after_filter \
                                + task_runtime

            elif "Stage" in event["Event"]:
                logger.debug("Found stage event: {}".format(event))
                stage_info = event["Stage Info"]

                if event["Event"] == "SparkListenerStageSubmitted":
                    nb_started_stages = nb_started_stages + 1

                elif event["Event"] == "SparkListenerStageCompleted":
                    nb_complete_stages = nb_complete_stages + 1
                    nb_tasks_todo = nb_tasks_todo + \
                        stage_info["Number of Tasks"]

    # TODO remove all tasks that are part of a stage that did not completedd
    if not executors:
        return None

    # add task numbers
    app_tasks_stat = {"app_id": app_id,
                      "app_type": app_type,
                      "nb_necessary_tasks": nb_tasks_todo,
                      "nb_started_tasks": nb_started_tasks,
                      "nb_complete_tasks": nb_complete_tasks,
                      "nb_failed_tasks": nb_failed_tasks,
                      "nb_resubmitted_tasks": nb_resubmitted_tasks,
                      "nb_started_stages": nb_started_stages,
                      "nb_complete_stages": nb_complete_stages,
                      "time_in_resubmitted_tasks": time_in_resubmitted_tasks,
                      "time_in_resubmitted_tasks_after_filter": time_in_resubmitted_tasks_after_filter,
                      "time_in_failed_task": time_in_failed_task,
                      "time_in_failed_task_after_filter": time_in_failed_task_after_filter,
                      "time_in_complete_task": time_in_complete_task,
                      "time_in_complete_task_after_filter": time_in_complete_task_after_filter,
                      "longest_task_execution_time": longest_task_execution_time,
                      **locality
                      }

    event_df = pd.DataFrame()
    for executor in executors.values():
        event_df = event_df.append(pd.DataFrame([executor]), ignore_index=True)

    # Convert timestamps from milisec to sec
    event_df["starting_time"] = event_df["starting_time"] // 1000
    event_df["finish_time"] = event_df["finish_time"] // 1000
    event_df["submission_time"] = event_df["submission_time"] // 1000
    logger.debug(event_df)
    return event_df, app_tasks_stat


def load_json_file(path):
    with open(path) as event_file:
        obj = json.load(event_file)
    return obj


def compute_results(result_path, workload_name, filter_after):
    log_dir = result_path + "/" + workload_name
    logger.info("Application log directory: {}".format(log_dir))
    app_logs = glob.glob(log_dir + "/application_*_????")
    logger.debug("Application list: {}".format(app_logs))

    nodes = load_json_file(result_path + "/nodes_info.json")

    loop = asyncio.get_event_loop()
    tasks = [get_events(app_log, nodes, filter_after) for app_log in app_logs]
    results = loop.run_until_complete(asyncio.gather(*tasks))

    #filter None results that contains no executors
    results = [x for x in results if x is not None]

    logger.info("Aggregating results...")
    events_list = [event_df for event_df, app_tasks_stat in results]
    app_stat_list = [pd.DataFrame([app_tasks_stat]) for event_df, app_tasks_stat in results]
    events_df = pd.concat(events_list, ignore_index=True)
    app_df = pd.concat(app_stat_list, ignore_index=True)

    return events_df, app_df


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('result_path')
    parser.add_argument('workload_name')
    parser.add_argument('output_file_jobset')
    parser.add_argument('output_file_appstat')
    parser.add_argument('-f', '--filter_after', type=float,
                        default=None,
                        help='Filter tasks that started after the filter time')
    args = parser.parse_args()

    # Get results
    log_dir = args.result_path + "/" + args.workload_name
    #extract_application_zip_logs(log_dir)
    events_df, app_df = compute_results(args.result_path,
                                        args.workload_name,
                                        args.filter_after)

    #print("Events DataFrame: {}".format(events_df))
    #print("Application stat DataFrame: {}".format(app_df))

    if events_df.empty:
        logger.error("No events found, abort!")
        sys.exit(1)
    js = JobSet(events_df)
    js.to_csv(args.output_file_jobset)

    app_df.to_csv(args.output_file_appstat)

    logger.info("Processing is finished")
    # plot results
    #js.plot(with_details=True, time_scale=True)
    # nrows = 4
    # _, axe = plt.subplots(nrows=nrows, sharex=True)
    # visu.plot_load(js.utilisation, js.MaxProcs,
    #                load_label="utilisation", ax=axe[0],
    #                time_scale=True)
    # visu.plot_load(js.queue, js.MaxProcs,
    #                load_label="queue", ax=axe[1],
    #                time_scale=True)
    # visu.plot_job_details(js.df, js.MaxProcs, ax=axe[2],
    #                       time_scale=True)

    # def color_app(job, palette):
    #     return palette[int(job.app_id.split('_')[2]) % len(palette)]

    # def label_fn(job):
    #     _, _, app, executor = job["jobID"].split('_')
    #     return app + "_" + executor
    # visu.plot_gantt(js, ax=axe[3], time_scale=True, color_function=color_app,
    #                 labels=False)
    #                 # label_function=label_fn)
    # plt.show()
