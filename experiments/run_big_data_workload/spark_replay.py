#!/usr/bin/env python3
'''
This script is taking a Batsim profile as input and replay the profile
using the provided commands on spark over YARN
'''
import argparse
import os
import json
import csv
import re
import subprocess
import asyncio
import aiofiles
import sys
import copy
import time
from urllib.request import urlopen
from urllib.error import URLError
import logging
import signal
import functools


logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)7s - %(message)s',
        stream=sys.stdout,
)
logger = logging.getLogger(__name__)


def shutdown(loop, result_dir):
    logging.info('received stop signal, cancelling tasks...')
    for task in asyncio.Task.all_tasks():
        task.cancel()
    logging.info('Gathering all logs')


def gather_bigdata_jobs_info(result_dir, app_id,
                             pull_timer=1,
                             master_addr="127.0.0.1"):

    logger.info("Waiting completion for Application id: {}".format(app_id))
    # wait for the job to finish
    is_completed = False
    while not is_completed:
        time.sleep(pull_timer)
        url = "http://{}:18080/api/v1/applications/?status=running".format(
            master_addr
        )
        app_status = urlopen(url).readall().decode('utf-8')
        logger.debug(app_status)
        is_completed = (not app_status) or (app_id not in app_status)

    logger.info("Gathering information for Application id: {}".format(app_id))

    try:
        subprocess.check_call(
            "$HADOOP_HOME/bin/hdfs dfs -copyToLocal "
            "/user/spark/applicationHistory/{} {}".format(app_id, result_dir),
            shell=True)

    except Exception as e:
        logger.error("Error while retrieving application logs: "
                     "{}".format(e))
    # get all logs from the job
    #url = "http://{}:18080/api/v1/applications/{}/1/logs".format(
    #    master_addr, app_id
    #)
    #CHUNK = 6 * 1024
    #with open("{}/{}.zip".format(result_dir, app_id), "wb") as myfile:
    #try:
    #    with urlopen(url) as response:
    #        while True:
    #            chunk = response.read(CHUNK)
    #            if not chunk:
    #                break
    #            myfile.write(chunk)
    #except Exception as e:
    #    logger.error("Error while retrieving application logs: "
    #                 "{}".format(e))

    logger.info("Info gathering is finished for Application: {} ".format(app_id))


@asyncio.coroutine
def start_BigDataBench_application(expe, delay, output_dir, job_id, writer,
                                   map_file):

    # wait until delay is past to start
    yield from asyncio.sleep(delay)


    expe["job_id"] = job_id

    # FIXME only done once because same profile is only expanded once
    # expand parameters
    expe["application_parameters"] = expe["application_parameters"].format(
            **expe)

    # Submit a Spark job command
    spark_submit_cmd = """/opt/spark/bin/spark-submit \
--master yarn \
--deploy-mode client \
--num-executors {nb_executor} \
--class cn.ac.ict.bigdatabench.{application_name} \
/opt/BigDataBench_V3.2.5_Spark/JAR_FILE/\
bigdatabench-spark_1.3.0-hadoop_1.0.4.jar \
/user/{user}/data-{application_type}-{data_size} \
{application_parameters}""".format(
        **expe)

    logger.info(spark_submit_cmd)
    stdout = '{}/spark_job_{}.out'.format(output_dir, job_id)
    stderr = '{}/spark_job_{}.err'.format(output_dir, job_id)
    stdout_file = open(stdout, "w+", buffering=1)
    stderr_file = open(stderr, "w+", buffering=1)

    create = asyncio.create_subprocess_shell(
        spark_submit_cmd,
        stderr=stderr_file,
        stdout=stdout_file
    )

    try:
        process = yield from create

        app_id = None
        logger.info("Started Spark submit process for job:{}".format(job_id))

        # Read stderr file to get the application ID
        stderr_async = yield from aiofiles.open(stderr, mode='r')
        try:
            while not app_id:
                # get out if the submission is finish without having an app id
                if process.returncode is not None:
                    logger.warn("Unable to schedule Spark application {}".format(job_id))
                    return
                line = yield from stderr_async.readline()
                if not line:
                    continue
                logger.debug(line)
                match = re.search('Submitted application (.*)$', line.strip())
                if match:
                    app_id = match.groups()[0]
                    break
                # Try to manage shutdown
                match = re.search('Shutdown hook called$', line.strip())
                if match:
                    logger.warn("Shutdown called in Spark application {}".format(job_id))
                    return
        finally:
            yield from stderr_async.close()

        logger.info("Application id: {}".format(app_id))

        # add mapping
        writer.writerow({"batsim_id": job_id, "spark_app_id": app_id})
        map_file.flush()

        yield from process.wait()
        stdout_file.flush()
        stdout_file.close()

        stderr_file.flush()
        stderr_file.close()
        logger.info("Process is finished for Application: {}".format(app_id))

    finally:
        # finish the application
        try:
            process.terminate()
        except ProcessLookupError:
            pass

        if app_id is not None:
            # Get logs of the application
            gather_bigdata_jobs_info(output_dir, app_id)


def do_replay(workload, jobs, profiles, result_dir, output_dir):
    output_dir = result_dir + "/" + output_dir
    try:
        os.mkdir(output_dir)
    except OSError as exc:
        if exc.errno != os.errno.EEXIST:
            raise exc

    # prepare output file
    mapping_file = '{}/{}-job_id_mapping.csv'.format(output_dir, workload)
    csv_file = open(mapping_file, 'w', buffering=1)
    try:
        writer = csv.DictWriter(csv_file, fieldnames=["batsim_id",
                                                      "spark_app_id"])
        writer.writeheader()
        to_run = []
        for job in jobs:
            # get command
            job_id = workload + "#" + str(job['id'])
            # all job have the same priority = 0
            to_run.append(
                    start_BigDataBench_application(
                        copy.deepcopy(profiles[job["profile"]]),
                        job['subtime'],
                        output_dir,
                        job_id,
                        writer,
                        csv_file
                    )
            )
        # run the jobs
        loop = asyncio.get_event_loop()
        # Add handler to manage termination
        loop.add_signal_handler(signal.SIGHUP,
                                functools.partial(shutdown, loop, result_dir))
        loop.add_signal_handler(signal.SIGTERM,
                                functools.partial(shutdown, loop, result_dir))

        # loop.set_debug(True)
        loop.run_until_complete(asyncio.wait(to_run))
    finally:
        csv_file.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Replay Batsim profile using'
                                                 'spark submitions')
    parser.add_argument('workload',
                        type=argparse.FileType('r'),
                        help='The input JSON Batsim workload file')
    parser.add_argument('result_dir',
                        help='The result directory to put the result and find '
                        'the hostname')
    parser.add_argument('output_dir',
                        help='The output folder.')

    args = parser.parse_args()

    # init script tool
    # expe_tools.script_path = script_path
    # parse the workload to get jobs
    json_data = json.load(args.workload)
    assert('profiles' in json_data), ("Invalid input file: It must contains a"
                                      "'profiles' map")

    # get informations
    workload = os.path.splitext(os.path.basename(args.workload.name))[0]
    jobs = json_data['jobs']
    profiles = json_data['profiles']
    result_dir = args.result_dir
    output_dir = args.output_dir

    logger.info("Start the replay for workload: {}".format(workload))
    do_replay(workload, jobs, profiles, result_dir, output_dir)
    logger.info("End of the replay for workload: {}".format(workload))
