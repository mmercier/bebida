from evalys import workload
from evalys.metrics import load_mean
import pandas as pd
from glob import glob
import matplotlib.pyplot as plt


def compute_overall_utilisation():
    '''
    Use all the swf file present in the curent directory to compute
    overall utilisation of the clusters.
    Write results to "overall_utilisation.csv" and return it.
    '''
    files = [f for f in glob('./swf_files/*.swf')]
    res = pd.DataFrame(columns=["norm_util_mean", "empty_queue_ratio"])
    for f in files:
        print('Loading SWF file: {}'.format(f))
        try:
            wl = workload.Workload.from_csv(f)
            if not hasattr(wl, "MaxProcs"):
                raise AttributeError("MaxProcs not provided!")
            norm_util_mean = load_mean(wl.utilisation) / wl.MaxProcs
            # compute time with empty queue over total time %
            q = wl.queue.reset_index()
            q0 = q[q.area == 0]
            zero_event_time = q0["time"].reset_index()
            after_zero_event_time = q.loc[q0.index + 1]["time"].reset_index()
            zero_time = (after_zero_event_time - zero_event_time).time.sum()
            total_time = \
                wl.utilisation.iloc[-1].name - wl.utilisation.iloc[0].name
            empty_queue_ratio = zero_time / total_time
            print('Mean util: {}\nempty queue ratio: {}\n'.format(
                norm_util_mean, empty_queue_ratio))
            res.loc[f] = [norm_util_mean, empty_queue_ratio]
        except AttributeError as e:
            print("Unable to compute normalize mean: {}".format(e))
        finally:
            if wl:
                del wl
    print('{}'.format(res))
    print(res.describe())
    res.to_csv("overall_utilisation.csv")
    return res


def plot_overall_utilisation(ou):
    # fig, axes = plt.subplots(nrows=2, sharex=True)
    # i = 0

    ou.reset_index().plot(x='name', y=['norm_util_mean', 'empty_queue_ratio'], kind='bar')
    plt.xticks(rotation=25)
    #for metric in ["norm_util_mean", "empty_queue_ratio"]:
    #    ax = axes[i]
    #    ax.axvline(ou[metric].mean(),
    #               linestyle='dashed',
    #               linewidth=2, label="mean " + metric)
    #    ou[metric].plot(kind="hist", ax=ax, label=metric)
    #    i = i+1
    #    plt.legend()

    plt.savefig("overall_util_hist.pdf")
    plt.show()


if __name__ == "__main__":
    import os.path
    if os.path.isfile("overall_utilisation.csv"):
        ou = pd.DataFrame.from_csv("overall_utilisation.csv")
    else:
        ou = compute_overall_utilisation()
    plot_overall_utilisation(ou)
